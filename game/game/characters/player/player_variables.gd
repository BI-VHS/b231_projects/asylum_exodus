extends Node

## Source of inspiration: https://docs.godotengine.org/en/stable/tutorials/physics/using_character_body_2d.html
@export var MAX_STAMINA: int = 500 
@export var speed: int = 300
@export var sprint_speed: int = 600
@export var NORMAL_PITCH_SCALE: float = 1
@export var SPRINTING_PITCH_SCALE: float = 1.2

@export var SPRINTING_INCREASE: int = 2
@export var SPRINTING_DECREASE: int = 1

var sprint_stamina: int = MAX_STAMINA
var is_resting: bool = false
var is_moving: bool = false
var can_interact: bool = false

# the difference between both scenes is exactly 10k pixels
const MAP_OFFSET = 10000

var current_floor: String = "ground"
