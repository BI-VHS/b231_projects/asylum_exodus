@tool
class_name PlayerCaughtHandler
extends Node

const MAX_STRIKES: int = 3

@export var where_to_port_after_caught: Node2D
@export var player: Player

var strikes: int = 0
@onready var label: Label = $CanvasLayer/Label
@onready var fade_timer: Timer = $TextFadeTimer
@onready var game_over_panel: Panel = $CanvasLayer/GameOverPanel
@onready var exit_button: Button = $CanvasLayer/Exit


func _ready() -> void:
	MessageBus.player_caught.connect(_on_player_caught)
	# MessageBus.endgame.connect(endgame)
	label.hide()
	game_over_panel.hide()
	exit_button.hide()


func _on_player_caught() -> void:
	player.global_position = where_to_port_after_caught.global_position
	strikes += 1
	if strikes >= MAX_STRIKES:
		# endgame()
		MessageBus.endgame.emit(Endgame.Reason.Catch)
	else:
		label.show()
		label.text = ("Byl si chycen!\n" + str(MAX_STRIKES - strikes) + " životy zbýbají!")
		fade_timer.start()

func endgame():
	label.show()
	game_over_panel.show()
	exit_button.show()
	exit_button.grab_focus()
	label.text = "KONEC HRY"
	get_tree().paused = true
	

func _on_text_fade_timer_timeout():
	label.hide()
	label.text = ""


func _on_exit_pressed():
	get_tree().paused = false
	get_tree().quit()
