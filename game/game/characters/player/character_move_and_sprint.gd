class_name Player
extends CharacterBody2D

@onready var animated_sprite: AnimatedSprite2D = $"AnimatedSprite"
@onready var animation_player: AnimationPlayer = $"AnimationPlayer"
@onready var actionable_finder: ActionableFinder = %ActionableFinder
@onready var footsteps: AudioStreamPlayer2D = $"Footsteps"
# @onready var npc_type = global_variables.NPCType.Patient
@onready var paused: bool = false


func _ready() -> void:
	animated_sprite.play()
	actionable_finder.enable()


func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("interact") and not paused:
		actionable_finder.interact_with_closest_actionable()
	elif event.is_action_pressed("toggle_fullscreen"):
		var mode = DisplayServer.window_get_mode()
		if mode == DisplayServer.WINDOW_MODE_FULLSCREEN:
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_MAXIMIZED)
		elif mode == DisplayServer.WINDOW_MODE_WINDOWED or mode == DisplayServer.WINDOW_MODE_MAXIMIZED:
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
		

func _physics_process(_delta):
	_handle_input()
	move_and_slide()


## Get input, and move player, play sounds and animations if needed
func _handle_input() -> void:
	if paused:
		return
	var input_dir: Vector2 = Input.get_vector("move_left", "move_right", "move_up", "move_down")
	var sprinting: bool = Input.is_action_pressed("sprint")

	_handle_footsteps(input_dir, sprinting)
	_handle_sprint_stamina()
	_handle_animation(input_dir)
	_handle_floor_movement()

	if sprinting && input_dir && !player_variables.is_resting:
		velocity = input_dir * player_variables.sprint_speed
		player_variables.sprint_stamina -= player_variables.SPRINTING_DECREASE
	else:
		velocity = input_dir * player_variables.speed
		if player_variables.sprint_stamina < player_variables.MAX_STAMINA:
			player_variables.sprint_stamina += player_variables.SPRINTING_INCREASE


## Play audio for footsteps if present, fasten if sprinting
func _handle_footsteps(input_dir: Vector2, sprinting: bool) -> void:
	if sprinting:
		footsteps.pitch_scale = player_variables.SPRINTING_PITCH_SCALE
	else:
		footsteps.pitch_scale = player_variables.NORMAL_PITCH_SCALE
	if input_dir:  # moving
		if not player_variables.is_moving:
			footsteps.play()
		player_variables.is_moving = true
	else:
		if player_variables.is_moving:
			footsteps.stop()
		player_variables.is_moving = false


## Update resting bool
func _handle_sprint_stamina() -> void:
	if player_variables.sprint_stamina < 0:
		player_variables.is_resting = true
	elif (
		player_variables.is_resting
		&& player_variables.sprint_stamina >= player_variables.MAX_STAMINA
	):
		player_variables.is_resting = false  # enable sprint only after full recovery


## Play animations depending on the direction of travel
##
## Prioritize vertical movement, when moving diagonally
func _handle_animation(input_dir: Vector2) -> void:
	if input_dir.length() == 0:
		animation_player.play("idle")
	elif input_dir.y > 0:
		animation_player.play("run_front")
	elif input_dir.y < 0:
		animation_player.play("run_back")
	elif input_dir.x > 0:
		animation_player.play("run_right")
	elif input_dir.x < 0:
		animation_player.play("run_left")

func teleport_to(new_position: Vector2) -> void:
	position = new_position

func _handle_floor_movement():
	if !player_variables.can_interact || !Input.is_action_just_pressed("interact"):
		return

	# switch to new flooraw
	match player_variables.current_floor:
		"ground":
			# temporarily turn off the collision mask
			#collision_mask = 0
			# move player 10k to the right
			position[0] += player_variables.MAP_OFFSET

			#collision_mask = 1
			player_variables.current_floor = "first"
		"first":
			# temporarily turn off the collision mask
			collision_mask = 0
			# move player 10k to the left
			position[0] -= player_variables.MAP_OFFSET
			#turn on collision mask
			collision_mask = 1
			player_variables.current_floor = "ground"


func _on_key_area_entered(_area) -> void:
	print("pick keey")


func is_patient() -> bool:
	return true
	
func is_player() -> bool:
	return true

func _on_teleport_timeout():
	#get_parent().add_child(self)
	print("Starting collisions again")
	collision_mask = 1
