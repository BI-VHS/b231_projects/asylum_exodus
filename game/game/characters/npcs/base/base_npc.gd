@tool
class_name BaseNpc
extends CharacterBody2D

## Provides the ability to navigate to a location and play animation
## of walking there and also show text on top of the NPC.
## For this base to work, please provide in your scene own:
##   AnimationPlayer with animations named run_[left, right, front, back]
##   NavigationAganet2D
##   Label named Speechlabel preferable on top of sprite of the NPC

## In seconds
const DEFAULT_SPEECH_SHOW_TIME: float = 3

@export var movement_speed: int

@export var animation_player: AnimationPlayer
@export var navigation_agent: NavigationAgent2D
@export var speech_label: Label

@onready var speech_timer: Timer = $SpeechTimer


func show_speech(message: String, time: float = DEFAULT_SPEECH_SHOW_TIME) -> void:
	speech_label.show()
	speech_label.text = message
	speech_timer.start(time)


func hide_speech() -> void:
	speech_label.text = ""
	speech_label.hide()
	speech_timer.stop()


func navigate_to_position(point: Vector2, _delta: float) -> void:
	navigation_agent.target_position = point
	if navigation_agent.is_navigation_finished():
		return

	var current_agent_position: Vector2 = global_position
	var next_path_position: Vector2 = navigation_agent.get_next_path_position()

	velocity = current_agent_position.direction_to(next_path_position) * movement_speed

	move_and_slide()
	_animate_walking(velocity)


func _animate_walking(movement_velocity: Vector2) -> void:
	var direction: Direction = Direction.vector_to_direction(movement_velocity)

	if direction.is_left():
		animation_player.play("run_left")
	elif direction.is_right():
		animation_player.play("run_right")
	elif direction.is_up():
		animation_player.play("run_back")
	elif direction.is_down():
		animation_player.play("run_front")


func _on_speech_timer_timeout() -> void:
	hide_speech()


## Editor warnings
func _get_configuration_warnings() -> PackedStringArray:
	var warning: PackedStringArray = []
	if animation_player == null:
		warning.push_back("BaseNpc requires AnimationPlayer node to be assigned!")
	if not animation_player == null:
		for animation_name in ["run_left", "run_right", "run_front", "run_back"]:
			if not animation_player.has_animation(animation_name):
				warning.push_back(
					"BaseNpc's AnimationPlayer lacks the animation named " + animation_name
				)
	if navigation_agent == null:
		warning.push_back("BaseNpc requires NavigationAgent2D node to be assigned!")
	if speech_label == null:
		warning.push_back("BaseNpc requires Label node used for speech to be assigned!")

	return warning
