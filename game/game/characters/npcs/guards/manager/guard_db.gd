class_name GuardDB
extends RefCounted

## Stores all information regarding guards and their duties

enum GuardId {
	DRYMORE,
	SWANWICK,
	CUNNINGHAM,
	HARRINGTON,
}

enum DutyId {
	FF_WEST_HALL,  ## First floor
	FF_EAST_HALL,
	FF_NORTH_HALL,
	FF_SOUTH_HALL,
	RECEPTION,
	KITCHEN,
	CANTEEN,
	OFFICES,
	GF_WEST_HALL,  ## Ground floor
	FF_HALL,
	GF_HALL,
	COMMON_ROOM_INSIDE,
	COMMON_ROOM_OUTSIDE,
	CANTEEN_INSIDE,
	CANTEEN_OUTSIDE,
	LIBRARY,
	WORKSHOP,
	WC_OUTSIDE,
	SLEEPING_ROOM_OUTSIDE,
	SECURITY_SLEEPING_ROOM_1,
	SECURITY_SLEEPING_ROOM_2,
}

## GuardId -> BaseGuard
var _guards: Dictionary = {}

## DutyId -> GuardDuty
var _duties: Dictionary = {}


func get_guard_by_id(guard_id: GuardId) -> BaseGuard:
	return _guards[guard_id]


func get_duty_by_id(duty_id: DutyId) -> GuardDuty:
	return _duties[duty_id]


func add_guards(guards: Array[BaseGuard]) -> void:
	for guard: BaseGuard in guards:
		_guards[guard.id] = guard


## If provided an active node, will ask SceneTree for all Guards in group "guards"
func add_guards_by_group(node: Node) -> void:
	add_guards(_get_guards_by_group(node))


func add_duties(duties: Array[GuardDuty]):
	for duty: GuardDuty in duties:
		_duties[duty.id] = duty


func _get_guards_by_group(node: Node) -> Array[BaseGuard]:
	var res: Array[BaseGuard] = []
	for guard in node.get_tree().get_nodes_in_group("guards"):
		res.push_back(guard)
	return res
