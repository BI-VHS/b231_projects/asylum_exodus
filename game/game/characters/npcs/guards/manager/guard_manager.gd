class_name GuardManager
extends Node

## Assigns guards to their shifts.
## The key idea is:
## Duties are converted and stored in DB, Guards are gathered by using their
## group and stored in DB too. Then a schedule is assigned, which the manager
## uses to assign duties to guards for defined durations.
## Note: At the moment we only consider an assignment's 'to' time, since
##       if there were to be a gap betwee current time and 'from' time, we
##       would have to define some default behaviour for that gap. Also
##       for the last GuardAssignment no callback/timer is set, since
##       we depend on the day ending a schedule resetting/being changed.

## Must always start day with a set schedule
@export var schedule: GuardSchedule

## Stores Guards and Duties by their IDs
var guard_db: GuardDB = GuardDB.new()

## Sets it up for every guard added.
##Can very well be null, then default behaviour of guard is used!
@export var zone_manager: ProhibitedZoneManager
@export var guard_path_holder: Node

## GuardId -> ActiveShift
var _active_shifts: Dictionary = {}

## GuardId -> AssignmentTimer
## Assigns each guard his timer
var _timers: Dictionary = {}

@onready var _timers_node: Node = $Timers

func _ready() -> void:
	GlobalTime.day_end.connect(start_day)

func start_day() -> void:
	_reset_active_shifts()
	for shift in schedule._shifts:
		_activate_shift(shift)


func add_duties(duties: Array[GuardDuty]) -> void:
	guard_db.add_duties(duties)


func add_guards(guards: Array[BaseGuard]) -> void:
	for guard in guards:
		guard.zone_manager = zone_manager
	guard_db.add_guards(guards)


func _activate_shift(shift: GuardShift) -> void:
	var active_shift := ActiveGuardShift.new(shift, guard_db.get_guard_by_id(shift.guard_id))
	_active_shifts[shift.guard_id] = active_shift

	_initialize_next_guard_assignment(active_shift)


## Sets the active_shift's guard's duty. If it is not the last assignment in a
## guard's shift, then also starts a timer for said assignment's end.
func _initialize_next_guard_assignment(active_shift: ActiveGuardShift) -> void:
	var new_assignment: GuardAssignment = active_shift.get_next_suitable_assignment(
		_get_time_since_midnight()
	)

	var guard: BaseGuard = active_shift.guard  ## cached
	var duty: GuardDuty = guard_db.get_duty_by_id(new_assignment.duty_id)
	_set_guards_duty(guard, duty)

	if active_shift.is_last_assignment():  ## No need to start timer
		return

	_set_assignment_timer(active_shift.guard_id, new_assignment.time_range.to)


func _initialize_next_guard_assignment_by_id(guard_id: GuardDB.GuardId):
	_initialize_next_guard_assignment(_active_shifts[guard_id])


## Sets-up callback to initialize next assignment after the current one finishes.
## If guard doesn't have a timer of his own, initializes a new timer for him,
## otherwise uses the one created before.
func _set_assignment_timer(guard_id: GuardDB.GuardId, until: int) -> void:
	var timer: Timer
	if not _timers.has(guard_id):
		timer = AssignmentTimer.new(guard_id)
		_timers[guard_id] = timer
		_timers_node.add_child(timer)
	else:
		timer = _timers[guard_id]

	timer.start(_how_long_until_in_real_time(until))
	timer.assignment_timeout.connect(_initialize_next_guard_assignment_by_id)


func _set_guards_duty(guard: BaseGuard, duty: GuardDuty) -> void:
	guard.set_guard_duty(duty)


## Resets the variables needed to operate a schedule
func _reset_active_shifts() -> void:
	for timer: Timer in _timers.values():
		timer.stop()

	_active_shifts.clear()  ## ActiveShift is RefCounted, so this is fine


func _on_day_end() -> void:
	start_day()


## Gets time since midnight
func _get_time_since_midnight() -> int:
	return GlobalTime.get_minutes_since_midnight()


## Asks the time singleton how many real life seconds before the given time is reached
func _how_long_until_in_real_time(_time: int) -> int:
	return GlobalTime.how_long_since_now_in_real_time_in_single_day(_time)


## DEBUG
func _print_schedule(schedule: GuardSchedule):
	var print_shift = func(shift: GuardShift):
		var print_assignment = func(assignment: GuardAssignment):
			print("  Assignment duty_id: ",assignment.duty_id, ", ", assignment.time_range.from, " - ", assignment.time_range.to)
		print("Shift of guard ", shift.guard_id, " assignments:")
		for assignment in shift._assignments:
			print_assignment.call(assignment)
	
	for shift in schedule._shifts:
		print_shift.call(shift)
