extends CharacterBody2D

@export var speed = 50

@export var target: Node2D
@onready var nav_agent := $NavigationAgent2D as NavigationAgent2D

func _process(delta):
	var dir = to_local(nav_agent.get_next_path_position()).normalized()
	velocity = dir * speed
	
	move_and_slide()
	
func makepath():
	nav_agent.target_position = target.position


func _on_timer_timeout():
	makepath()
