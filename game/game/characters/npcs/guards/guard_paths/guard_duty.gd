class_name GuardDuty
extends RefCounted

## Holds data defining a route for a guard to safeguard. Please assign one copy
## of a GuardDuty to each Guard, don't let them share it, because GuardDuty also
## takes care of iterating through tasks for ease of use

var id: GuardDB.DutyId

var _task_index: int = 0
var _tasks: Array[GuardTask]


## Get next task and move the task iterator forward. Loop to first task if there
## are no more tasks to give out
func get_next_task() -> GuardTask:
	if _task_index >= _tasks.size():
		_task_index = 0

	var task: GuardTask = _tasks[_task_index]
	_task_index = _task_index + 1
	return task


func copy() -> GuardDuty:
	return GuardDuty.new(_tasks, id)


func _init(tasks: Array[GuardTask], duty_id: GuardDB.DutyId) -> void:
	_tasks = tasks
	id = duty_id
