extends Sprite2D

const MOVEMENT_SPEED: int = 200

@onready var guard_duty_factory: GuardDutyFactory = $GuardDutyFactory
@onready var navigation_agent: NavigationAgent2D = $MovingObject/NavigationAgent2D
@onready var standing_timer: Timer = $Timer
@onready var moving_object: Sprite2D = $MovingObject

var guard_duty: GuardDuty
var current_task: GuardTask
var duty_started: bool = false


## Example checking if guard factory works
func _ready() -> void:
	guard_duty = guard_duty_factory.create_guard_duty_from_children()
	current_task = guard_duty.get_next_task()


func _process_goto_task(delta: float) -> void:
	if !duty_started:
		duty_started = true
		navigation_agent.target_position = current_task.target
		print("Target: ", current_task.target)

	if navigation_agent.is_navigation_finished():
		print("navigation finished")

	var current_agent_position: Vector2 = moving_object.global_position
	var next_path_position: Vector2 = navigation_agent.get_next_path_position()

	var new_velocity = next_path_position - current_agent_position
	new_velocity = new_velocity.normalized()
	new_velocity = new_velocity * MOVEMENT_SPEED

	moving_object.position = moving_object.position + new_velocity * delta


func _process_stand_task(delta: float) -> void:
	if !duty_started:
		duty_started = true

		if current_task.direction.is_down():
			moving_object.rotation_degrees = 0
		if current_task.direction.is_up():
			moving_object.rotation_degrees = 180
		if current_task.direction.is_left():
			moving_object.rotation_degrees = -90
		if current_task.direction.is_right():
			moving_object.rotation_degrees = 90

		standing_timer.wait_time = current_task.duration
		standing_timer.start()


func _physics_process(delta: float) -> void:
	if current_task is GuardGotoTask:
		print("GotoTask: ", current_task.target)
		print("Position: ", moving_object.position)
		_process_goto_task(delta)
	elif current_task is GuardStandTask:
		print("StandTask: ", current_task.direction, current_task.duration)
		_process_stand_task(delta)


func _set_new_task() -> void:
	print("changing duty")
	duty_started = false
	current_task = guard_duty.get_next_task()


func _on_timer_timeout() -> void:
	_set_new_task()


func _on_navigation_agent_2d_navigation_finished() -> void:
	_set_new_task()
