extends GuardTask

## Holds data necessary to navigate guard to next waypoint on his guard route
class_name GuardGotoTask


var target: Vector2


func _init(_target: Vector2) -> void:
	target = _target
