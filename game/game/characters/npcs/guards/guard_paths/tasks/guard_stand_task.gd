extends GuardTask

## Container for information on where a guard should look and for how long on his route
class_name GuardStandTask


var direction: Direction
var duration: int


func _init(_direction: Direction, _duration: int) -> void:
	direction = _direction
	duration = _duration

