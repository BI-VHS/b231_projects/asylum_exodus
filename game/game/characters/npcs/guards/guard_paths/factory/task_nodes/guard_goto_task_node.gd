extends Node2D

## Class for in-editor definition of a waypoint for guards to goto. It is itself
## a Node2D, so position variable is already defined and should be used.
class_name GuardGotoTaskConfig
