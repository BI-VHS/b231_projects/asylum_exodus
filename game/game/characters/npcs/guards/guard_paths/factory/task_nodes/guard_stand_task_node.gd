extends Node


## Class for in-editor defining behaviour of a standing guard. In essence
## in what direction he should look and for how long should he be looking.
class_name GuardStandTaskConfig

@export var direction: Direction.DirectionEnum
@export var duration: int
