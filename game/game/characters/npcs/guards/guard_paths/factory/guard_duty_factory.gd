class_name GuardDutyFactory
extends Node

## When called creates GuardTasks from children nodes GuardTasks. Please provide
## only GuardGotTaskNode and GuardStandTaskNode as children.
## GuardGotoTask -> defines where the guard should go to next
## GuardStandTask -> use if we want the guard to stand in place and look in
##                   some direction for some amount of time.

## ID of duty created by this factory, always set this and make sure it is unique!
@export var duty_id: GuardDB.DutyId


## Creates RefCounted-based containers defining guard's tasks from their Node representation
func create_guard_duty_from_children() -> GuardDuty:
	var arr: Array[GuardTask]

	for child in get_children():
		if child is GuardGotoTaskConfig:
			arr.push_back(GuardGotoTask.new(child.global_position))
		elif child is GuardStandTaskConfig:
			arr.push_back(GuardStandTask.new(Direction.new(child.direction), child.duration))
		else:
			assert(false)

	queue_free()
	return GuardDuty.new(arr, duty_id)
