@tool
class_name BaseGuard
extends BaseNpc

## Base class for all guards. Guards have different animation sprites,
## their behaviour is controlled using the StateChart and defined using
## GuardDuty made out of individual GuardTasks. In essence GuardDutyFactory node
## provides a way to create paths (GuardDuty), which guards follow in editor.
## The created GuardDuty is then given to the Guard, which he follows in a loop.
## Please provide for each individual guard variant its own AnimatedSprite2D,
## AnimationPlayer, VisionCone2D and COllisionShape. Also use standard animation names.
## Also please provide a SpeechTimer connected to _on_speech_timer_timeout.

@export var _wait_time_after_chase: float = 2.5  ## in seconds
@export var id: GuardDB.GuardId

@export var animated_sprite: AnimatedSprite2D
@export var vision_cone: VisionCone2D

@export_subgroup("Guard speech texts")
@export var on_patient_lost_in_prohibited_area_speech: Array[String] = (
	["Asi vítr...",
	"Se mi něco asi zdálo...",
	"Určitě jsem něco zahlédl..."]
)
@export var on_patient_back_in_allowed_area_speech: Array[String] = (
	["Tak a zůstaň tam!",
	"Dobrá, tady už můžeš být...",
	"A nedělej další problémy!"]
)
@export var on_patient_seen_trespassing_speech: Array[String] = (
	["Kdo je tam?!",
	"Je tam někdo?!",
	"Co tam děláš?!"]
)

## If no zone_manager is set, the default behaviour is to chase always regrdless of zone.
var zone_manager: ProhibitedZoneManager

## Duty:
var _guard_duty: GuardDuty
var _current_task: GuardTask

## Vision:

var _vision_every_x_frames: int = 10
var _vision_every_x_frames_counter: int = 0

var tresspassing_player_override: Node2D = null

## Always generate a new array, in case zones change or whatever else
var _patients_trespassing_seen: Array[Node2D]:
	get:
		var bodies: Array[Node2D]
		for body in vision_cone.vision_cone_area.get_overlapping_bodies():
			if _is_body_a_patient_trespassing(body):
				bodies.push_back(body)
		if tresspassing_player_override != null:
			if _is_body_a_patient_trespassing(tresspassing_player_override):
				bodies.push_front(tresspassing_player_override)
		return bodies


## Chasing:
var _last_seen_body: Node2D
var _last_seen_point: Vector2
var _position_before_chase: Vector2

## Animations:
var _idle_animations: Array[String] = ["idle_left", "idle_right", "idle_back", "idle_front"]


@onready var state_chart: StateChart = $StateChart
@onready var stand_task_timer: Timer = $StandTaskTimer
@onready var after_chase_standing_timer: Timer = $AfterChaseStandingTimer
@onready var standing_around_animation_timer: Timer = $StandingAroundAnimationTimer


func _ready() -> void:
	animated_sprite.play()
	vision_cone.vision_cone_body_entered.connect(_on_vision_cone_body_entered)
	vision_cone.vision_cone_body_exited.connect(_on_vision_cone_body_exited)


## Replace guard's GuardDuty with new one, only public method
## Will immediately start working on the new duty or in other words the first task
func set_guard_duty(guard_duty: GuardDuty) -> void:
	#printerr("Guard duty: ", EnumPrintHelpers.name_by_id(guard_duty.id, GuardDB.DutyId) ," set to guard ", EnumPrintHelpers.name_by_id(id, GuardDB.GuardId))
	_guard_duty = guard_duty
	if _current_task == null:
		_current_task = _guard_duty.get_next_task()
	state_chart.send_event("new_duty")


#region Vision
## Whenever vision_cone detects a trespassing patient, notify state_chart about
## or there are no more trespassing patients, notify state_chart.


func _on_senses_physics_processing(_delta: float) -> void:
	_vision_every_x_frames_counter += 1
	if _vision_every_x_frames_counter < _vision_every_x_frames:
		return

	_vision_every_x_frames_counter = 0
	if _patients_trespassing_seen.size() == 0:
		state_chart.send_event("dont_see_patient_trespassing")
		movement_speed = 200
	else:
		state_chart.send_event("see_patient_trespassing")
		movement_speed = 300


## Add body to _patients_trespassing_seen, if it is a trespassing patient.
## Notify state_chart that there is at least one patient trespassing
func _on_vision_cone_body_entered(_body: Node2D) -> void:
	if _patients_trespassing_seen.size() > 0:
		state_chart.send_event("see_patient_trespassing")


## Remove body from _patients_trespassing_seen, if it was a trespassing patient.
## Notify state_chart if it was the last removed patient
func _on_vision_cone_body_exited(_body: Node2D) -> void:
	if _patients_trespassing_seen.size() == 0:
		_last_seen_point = _body.global_position
		_last_seen_body = _body
		state_chart.send_event("dont_see_patient_trespassing")


## Decides if a body is actually trespassing patient or not
func _is_body_a_patient_trespassing(body: Node2D) -> bool:
	return (body.has_method("is_patient")
		and body.is_patient()
		and (zone_manager == null ## If zone_manager is not defined, then always chase
		or zone_manager.is_body_in_prohibited_zone(body)))

#endregion


#region Guarding
## NoTask: is only active when _guard_duty == null and guard idles and slowly
##         and randomly changes direction in which he is looking
## GotoTask: Guard navigates to point stored in _current_task.target
## StandTask: _current_task needs to be of type GuardStandTask.
##            Guard stands in place and looks in _current_task.direction
##            for _current_task.duration seconds
## DecideNextTask: Guard should be in this state after end of every task and
##                 and instantly decide which activity he should do next


func _on_guarding_state_exited() -> void:
	_position_before_chase = global_position


#region NoTask
func _on_no_task_state_entered() -> void:
	print("No task entered")
	if _guard_duty != null:
		state_chart.send_event("new_duty")
	_play_different_standing_animation_with_timer(
		randf_range(1, 3), standing_around_animation_timer
	)


func _on_no_task_state_exited() -> void:
	standing_around_animation_timer.stop()

#endregion


#region GotoTask
func _on_goto_task_state_physics_processing(delta: float) -> void:
	navigate_to_position(_current_task.target, delta)

#endregion


#region DecideNextStateTask
func _on_decide_next_task_state_entered() -> void:
	_set_new_task()


## Called in DecideNextTask State, both updates _current_task and sends appropriate
## event to state_chart, triggering a transition to appropriate Guarding state
func _set_new_task() -> void:
	if _guard_duty == null:
		state_chart.send_event("no_task_next")
		return

	_current_task = _guard_duty.get_next_task()
	if _current_task is GuardGotoTask:
		state_chart.send_event("goto_task_next")
	if _current_task is GuardStandTask:
		state_chart.send_event("stand_task_next")

#endregion


#region StandTask
func _on_stand_task_state_entered() -> void:
	stand_task_timer.start(_current_task.duration)
	_animate_standing(_current_task.direction)


func _on_stand_task_timer_timeout() -> void:
	state_chart.send_event("stand_task_timer_timeout")

#endregion
#endregion


#region Chasing


#region ChasingFollowing
##Guard follows the closes trespassing patient he sees.


func _on_following_state_entered() -> void:
	show_speech(on_patient_seen_trespassing_speech.pick_random())

func _get_closest_trespassing_patient_position() -> Vector2:
	var patient_positions = _patients_trespassing_seen.map(
		func(patient: Node2D): return patient.global_position
	)
	var pick_closest = func(a: Vector2, b: Vector2): return ( ## ternary operator
		a if global_position.distance_to(a) < global_position.distance_to(b) else b
	)
	var closest = patient_positions.reduce(pick_closest)
	_last_seen_point = closest
	return closest


func _on_following_state_physics_processing(delta: float) -> void:
	if not _patients_trespassing_seen.is_empty():
		var closest_trepsassing_patient_position: Vector2 = _get_closest_trespassing_patient_position()
		navigate_to_position(closest_trepsassing_patient_position, delta)

#endregion


#region MovingToLastSeenPoint
## Guard navigates to last seen position of the inmate he was chasing last.

func _on_moving_to_last_seen_point_state_entered() -> void:
	if _last_seen_body == null:
		return

	if zone_manager == null || zone_manager.is_body_in_prohibited_zone(_last_seen_body):
		show_speech(on_patient_lost_in_prohibited_area_speech.pick_random())
	else:
		show_speech(on_patient_back_in_allowed_area_speech.pick_random())


func _on_moving_to_last_seen_point_state_physics_processing(delta: float) -> void:
	navigate_to_position(_last_seen_point, delta)

#endregion


#region StandingAtLastSeenPoint
## Guard looks around trying to locate the inmate he was chasing. First he
## looks in the direction he was going to the point for random amount of time.
## Then he looks in random directions for a random amount of time to make him
## look confused. After _wait_time_after_chase time has passed, the state ends.


func _on_standing_at_last_seen_point_state_entered() -> void:
	after_chase_standing_timer.start(_wait_time_after_chase)
	standing_around_animation_timer.start(randf_range(0.15, 0.30) * _wait_time_after_chase)
	match animation_player.current_animation:
		"run_left":
			_animate_standing(Direction.new_left())
		"run_right":
			_animate_standing(Direction.new_right())
		"run_back":
			_animate_standing(Direction.new_up())
		"run_front":
			_animate_standing(Direction.new_down())


func _on_standing_at_last_seen_point_event_received(event: StringName) -> void:
	if event == "standing_around_animation_timer_timeout":
		_play_different_standing_animation_with_timer(
			randf_range(0.15, 0.30) * _wait_time_after_chase, standing_around_animation_timer
		)


func _on_after_chase_standing_timer_timeout() -> void:
	state_chart.send_event("after_chase_standing_timer_timeout")


func _on_standing_around_animation_timer_timeout() -> void:
	state_chart.send_event("standing_around_animation_timer_timeout")


func _on_standing_at_last_seen_point_state_exited() -> void:
	after_chase_standing_timer.stop()
	standing_around_animation_timer.stop()

#endregion


#region GoingBackToDuty
func _on_going_back_to_duty_state_physics_processing(delta: float) -> void:
	navigate_to_position(_position_before_chase, delta)

#endregion
#endregion


#region Animation
func _animate_standing(direction: Direction = Direction.new_down()) -> void:
	if direction.is_left():
		animation_player.play("idle_left")
	elif direction.is_right():
		animation_player.play("idle_right")
	elif direction.is_up():
		animation_player.play("idle_back")
	elif direction.is_down():
		animation_player.play("idle_front")


func _get_different_animation(animations: Array[String], current: String) -> Array[String]:
	var excluded_animations := animations.duplicate()
	excluded_animations.erase(current)
	return excluded_animations


func _play_animation_with_timer(animation: String, time: float, timer: Timer):
	animation_player.play(animation)
	timer.start(time)


## Plays random animation from provided animations and starts
## provided timer for random time in seconds between min_time and max_time
func _play_random_animation_with_timer(
	animations: Array[String], time: float, timer: Timer
) -> void:
	var animation: String = animations[randi_range(0, animations.size() - 1)]
	_play_animation_with_timer(animation, time, timer)


## Plays random standing animation (one from _idle_animations array)
## excluding the one being currently played and starts provided timer
## for random time in seconds between min_time and max_time
func _play_different_standing_animation_with_timer(time: float, timer: Timer):
	_play_random_animation_with_timer(
		_get_different_animation(_idle_animations, animation_player.current_animation),
		time,
		timer
	)

#endregion


#region Navigation
func _on_navigation_agent_2d_navigation_finished() -> void:
	state_chart.send_event("navigation_finished")

#endregion

## Editor warnings
func _get_configuration_warnings() -> PackedStringArray:
	var warning: PackedStringArray = []
	if not animation_player == null:
		for animation_name in _idle_animations:
			if not animation_player.has_animation(animation_name):
				warning.push_back(
					"BaseGuard's AnimationPlayer lacks the animation named " + animation_name
				)
	if animated_sprite == null:
		warning.push_back("BaseGuard requires AnimatedSprite2D node to be assigned to it!")
	if vision_cone == null:
		warning.push_back("BaseGuard requires VisionCone2D node to be assigned to it!")
	warning.append_array(super._get_configuration_warnings())


	return warning
	
func _on_catch_hitbox_body_entered(_body):
	printerr("Collision with player")
	if _patients_trespassing_seen.size() > 0: ## At the moment only player is trespassing patient
		printerr("Player caught")
		MessageBus.player_caught.emit()





