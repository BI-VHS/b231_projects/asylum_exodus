class_name GuardSchedule
extends Resource

## Defines all shifts of all guards in a single day. Please insert shifts in order

@export var _shifts: Array[GuardShift] = []
