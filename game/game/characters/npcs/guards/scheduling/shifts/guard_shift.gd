class_name GuardShift
extends Resource

## One day's work of a guard

@export var guard_id: GuardDB.GuardId

@export var _assignments: Array[GuardAssignment]
