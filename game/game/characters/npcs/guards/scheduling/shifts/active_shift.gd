class_name ActiveGuardShift
extends RefCounted

## Caches a guard's information regarding a shift

var guard: BaseGuard

var guard_id: GuardDB.GuardId

var _assignments: Array[GuardAssignment] = []

var _current_index: int = -1


## Gets the next assignment, which has its time range end after given 'time'
func get_next_suitable_assignment(time: int) -> GuardAssignment:
	var new_assignment: GuardAssignment = get_new_assignment()
	if new_assignment.time_range.to > time:
		return new_assignment

	printerr("Wanted to get next assignment, but it was no longer valid, getting the next one!")
	printerr(
		(
			"Bad assignment: "
			+ str(new_assignment.time_range.from)
			+ " - "
			+ str(new_assignment.time_range.to)
		)
	)
	if is_last_assignment():
		return new_assignment

	return get_next_suitable_assignment(time)  ## recursion over state


## Gets next assignment, all calls to get_current_assignment after calling this function
## will return the same assignment as this function. Last assignment will be returned
## over and over if it truly is the last.
func get_new_assignment() -> GuardAssignment:
	if is_last_assignment():
		return get_current_assignment()
	_current_index += 1
	return _assignments[_current_index]


func get_current_assignment() -> GuardAssignment:
	return _assignments[_current_index]


func is_last_assignment() -> bool:
	print("IsLastAssignment: ", _current_index + 1, " >= ", _assignments.size())
	return _current_index + 1 >= _assignments.size()


func _init(shift: GuardShift, _guard: BaseGuard) -> void:
	guard = _guard
	guard_id = _guard.id
	_assignments = shift._assignments.duplicate()
	## Sorting here might be redundant, but safer ATM
	_assignments.sort_custom(
		func(a: GuardAssignment, b: GuardAssignment): return a.time_range.to < b.time_range.to
	)
