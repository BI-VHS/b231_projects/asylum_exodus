class_name AssignmentTimer
extends Timer

## Timer with extra signal, which remembers and emits back guard_id given at initialization

signal assignment_timeout(guard_id: GuardDB.GuardId)


func _init(guard_id: GuardDB.GuardId) -> void:
	timeout.connect(func(): assignment_timeout.emit(guard_id))
	one_shot = true
