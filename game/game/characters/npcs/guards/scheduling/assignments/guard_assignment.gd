class_name GuardAssignment
extends Resource

## A duty and a time range in which the duty is to be executed in
@export var duty_id: GuardDB.DutyId

var time_range: IntRange:
	get:
		return _get_range()

var _time_range_field: IntRange


func _get_range() -> IntRange:
	return _time_range_field


func _init(_range: IntRange, _duty_id: GuardDB.DutyId) -> void:
	_time_range_field = _range
	duty_id = _duty_id
