class_name GuardAssignmentWithNumbers
extends GuardAssignment

## Provides an interface for creating assignments allowing e.g. 12:00 - 14:30 KitchenDuty,
## but it is more safe, and more verbose than the variant using strings

@export_range(0, 23) var _from_hours: int
@export_range(0, 59) var _from_minutes: int

@export_range(0, 23) var _to_hours: int
@export_range(0, 59) var _to_minutes: int

var _range_gotten: bool = false


## Don't call, fixes bug of this class not being exported by editor if there is no _init
## defined god knows why.
func _init() -> void:
	pass


## Since exported stuff is not present in constructor, we have to do this
func _get_range() -> IntRange:
	if _range_gotten:
		return _time_range_field
	_range_gotten = true
	_time_range_field = _to_minute_range()
	return _time_range_field


func _to_minute_range() -> IntRange:
	return IntRange.new(_from_hours * 60 + _from_minutes, _to_hours * 60 + _to_minutes)
