class_name GuardAssignmentWithStrings
extends GuardAssignment

## Provides an interface for creating assignments allowing e.g. 12:00 - 14:30 KitchenDuty

## Please use this exact format hh:mm e.g. 15:30
@export var _from: String
## Please use this exact format hh:mm e.g. 15:30
@export var _to: String

var _range_gotten: bool = false


## Since exported stuff is not present in constructor, we have to do this
func _get_range() -> IntRange:
	if _range_gotten:
		return _time_range_field

	_range_gotten = true
	var from: String = _from + ":00"
	var to: String = _to + ":00"
	## Because we get seconds not minutes we need to divide by 60
	var from_int = _string_to_datetime(from) / 60
	var to_int = _string_to_datetime(to) / 60
	_time_range_field = IntRange.new(from_int, to_int)
	return _time_range_field


func _init() -> void:
	var from: String = _from + ":00"
	var to: String = _to + ":00"
	## Because we get seconds not minutes we need to divide by 60
	var from_int = _string_to_datetime(from) / 60
	var to_int = _string_to_datetime(to) / 60

	time_range = IntRange.new(from_int, to_int)


func _string_to_datetime(time_str: String) -> int:
	return Time.get_unix_time_from_datetime_string(time_str)
