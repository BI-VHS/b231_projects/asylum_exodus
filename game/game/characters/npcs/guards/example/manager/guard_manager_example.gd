extends Node2D

var _duties: Array[GuardDuty] = []
var _guards: Array[BaseGuard] = []

@onready var duty_factory: GuardDutyFactory = $GuardDutyFactory
@onready var base_guard: BaseGuard = $AiTest/BaseGuard
@onready var time_info: Label = %TimeInfo
@onready var guard_manager: GuardManager = $GuardManager


## Just assign GuardDuty to the Guard
func _ready() -> void:
	_duties = _get_duties($Duties)
	guard_manager.add_duties(_duties)

	for guard in get_tree().get_nodes_in_group("guards"):
		_guards.push_back(guard)

	guard_manager.add_guards(_guards)

	guard_manager.start_day()


func _physics_process(_delta: float) -> void:
	time_info.text = "Clock: " + GlobalTime.time_as_string(GlobalTime.get_minutes_since_midnight())


func _get_guards_by_group(node: Node) -> Array[BaseGuard]:
	var res: Array[BaseGuard] = []
	for guard in node.get_tree().get_nodes_in_group("guards"):
		res.push_back(guard)
	return res


func _get_duties(factory_holder: Node) -> Array[GuardDuty]:
	var factories: Array[GuardDutyFactory] = []
	for factory in factory_holder.get_children():
		factories.push_back(factory)

	var duties: Array[GuardDuty] = []
	for duty in factories.map(func(factory): return factory.create_guard_duty_from_children()):
		duties.push_back(duty)

	return duties
