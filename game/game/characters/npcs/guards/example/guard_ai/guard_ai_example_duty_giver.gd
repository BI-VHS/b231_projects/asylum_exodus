extends Node2D

var duty: GuardDuty

@onready var duty_factory: GuardDutyFactory = $GuardDutyFactory
@onready var base_guard: BaseGuard = $AiTest/BaseGuard


## Just assign GuardDuty to the Guard
func _ready() -> void:
	duty = duty_factory.create_guard_duty_from_children()
	base_guard.set_guard_duty(duty_factory.create_guard_duty_from_children().copy())


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("interact"):
		base_guard.set_guard_duty(duty.copy())
