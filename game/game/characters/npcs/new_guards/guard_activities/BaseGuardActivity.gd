@tool
class_name BaseGuardActivity
extends Node

var schedule: BaseNewGuardSchedule
@export var state_chart: StateChart

func emit_end():
	schedule.switch_activity.emit()

func start_activity():
	state_chart.send_event("start")
	
func end_activity():
	state_chart.send_event("end")
