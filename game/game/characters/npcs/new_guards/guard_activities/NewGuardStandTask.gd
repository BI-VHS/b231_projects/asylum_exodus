@tool
class_name NewGuardStandTask
extends BaseGuardActivity

var look_position: Vector2
@export var stand_time: float
@onready var timer = $WaitTimer

func _ready():
	timer.wait_time = stand_time

func get_position():
	look_position = schedule.guard.global_position

func _on_look_right_state_entered():
	get_position()
	look_position.x += 1

func _on_look_down_state_entered():
	get_position()
	look_position.y += 1

func _on_look_left_state_entered():
	get_position()
	look_position.x -= 1

func _on_look_up_state_entered():
	get_position()
	look_position.y -= 1

func _on_look_right_state_physics_processing(delta):
	schedule.guard.navigate_to_position(look_position, delta)
	if schedule.guard.navigation_agent.is_navigation_finished():
		schedule.guard.animate_idle(Direction.new_right())
		
		print("next look")
		state_chart.send_event("next")

func _on_look_down_state_physics_processing(delta):
	schedule.guard.navigate_to_position(look_position, delta)
	if schedule.guard.navigation_agent.is_navigation_finished():
		schedule.guard.animate_idle(Direction.new_down())
		
		print("next look")
		state_chart.send_event("next")

func _on_look_left_state_physics_processing(delta):
	schedule.guard.navigate_to_position(look_position, delta)
	if schedule.guard.navigation_agent.is_navigation_finished():
		schedule.guard.animate_idle(Direction.new_left())
		
		print("next look")
		state_chart.send_event("next")

func _on_look_up_state_physics_processing(delta):
	schedule.guard.navigate_to_position(look_position, delta)
	if schedule.guard.navigation_agent.is_navigation_finished():
		schedule.guard.animate_idle(Direction.new_up())
		
		print("last look")
		state_chart.send_event("next")


func _on_bruh_state_entered():
	emit_end()

func _on_wait_state_entered():
	timer.start()

func _on_wait_2_state_entered():
	timer.start()

func _on_wait_3_state_entered():
	timer.start()

func _on_wait_4_state_entered():
	timer.start()

func _on_wait_timer_timeout():
	state_chart.send_event("next")
