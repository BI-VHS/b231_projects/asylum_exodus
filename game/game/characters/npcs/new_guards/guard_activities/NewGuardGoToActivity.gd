@tool
class_name NewGuardGoToActivity
extends BaseGuardActivity

@export var go_to_position: Vector2


func _on_go_to_state_physics_processing(delta):
	schedule.guard.navigate_to_position(go_to_position, delta)
	if schedule.guard.navigation_agent.is_navigation_finished():
		print("guard nav finished")
		emit_end()
