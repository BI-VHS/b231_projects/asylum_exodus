@tool
class_name BaseNewGuardSchedule
extends Node

@export var guard: NewBaseGuard
@export var activities: Array[BaseGuardActivity]
@onready var current_activity_index: int = 0
@export var from: Vector2
@export var to: Vector2

signal switch_activity

func _ready():
	switch_activity.connect(switch_to_next_activity)
	for act in activities:
		act.schedule = self
	
func switch_to_next_activity():
	activities[current_activity_index].end_activity()
	current_activity_index += 1
	current_activity_index = current_activity_index % activities.size()
	print("Switching to new guard activity")
	print(current_activity_index)
	activities[current_activity_index].start_activity()

func start_schedule():
	current_activity_index = 0
	activities[current_activity_index].start_activity()
	
func resume_schedule():
	activities[current_activity_index].start_activity()
	
func end_schedule():
	activities[current_activity_index].end_activity()
