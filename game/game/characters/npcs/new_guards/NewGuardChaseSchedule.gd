@tool
class_name NewGuardChaseSchedule
extends BaseNewGuardSchedule

@onready var state_chart: StateChart = $StateChart
var last_position: Vector2

func start_schedule():
	state_chart.send_event("start")
	
func end_schedule():
	state_chart.send_event("end")

func _on_chasing_state_physics_processing(delta):
	if guard.chasing != null:
		guard.navigate_to_position(guard.chasing.global_position, delta)
		last_position = guard.chasing.global_position
	else:
		guard.navigate_to_position(last_position, delta)
