@tool
class_name NewBaseGuard
extends BaseNpc

# @onready var npc_type = global_variables.NPCType.Guard
# check patient with has_method(is_patient)
@onready var vision_cone = $VisionCone2D
@export var sprite: AnimatedSprite2D
@export var zone_manager: ProhibitedZoneManager
@export var schedule: Array[BaseNewGuardSchedule]
@onready var chasing_schedule: BaseNewGuardSchedule = $NewGuardChaseSchedule
@export var insta_end: bool = false
@onready var showing_text: bool = false

var current_time: Vector2
var last_index: int
var kickstart = true
var last_schedule: BaseNewGuardSchedule
@onready var chasing: Node2D = null


func get_current_schedule_index() -> int:
	var x: int = 0
	while x < schedule.size():
		var act = schedule[x]
		if current_time[0] >= act.from[0] and current_time[0] <= act.to[0]:
			if current_time[1] >= act.from[1] and current_time[1] <= act.to[1]:
				return x
		x += 1
	return last_index


func show_speech(message: String, time: float = DEFAULT_SPEECH_SHOW_TIME) -> void:
	if showing_text:
		return
	speech_label.text = message
	speech_label.show()
	showing_text = true
	await get_tree().create_timer(time).timeout
	speech_label.text = ""
	speech_label.hide()
	showing_text = false

func _on_ready():
	GlobalTime.update.connect(time_check)
	vision_cone.vision_cone_body_entered.connect(vision_cone_body_entered)
	vision_cone.vision_cone_body_exited.connect(vision_cone_body_exited)
	sprite.play("idle_front")
	
func vision_cone_body_entered(body: Node2D):
	if chasing != null:
		return
	if body.has_method("is_patient"):
		if body.is_patient() and zone_manager.is_body_in_prohibited_zone(body):
			chasing = body
			movement_speed = 350
			show_speech("Jen počkej!", 2)
	
func vision_cone_body_exited(body: Node2D):
	if body == chasing:
		chasing = null
		movement_speed = 250
	
func kickstart_activity():
		var current_schedule = schedule[get_current_schedule_index()]
		last_index = get_current_schedule_index()
		last_schedule = current_schedule
		current_schedule.start_schedule()
		kickstart = false
	
func time_check(new_time: Vector2):
	current_time = new_time
	if kickstart:
		kickstart_activity()
	
	if chasing != null:
		var current_schedule = chasing_schedule
		if last_schedule != current_schedule:
			print("starting pursuit")
			last_schedule.end_schedule()
			current_schedule.start_schedule()
			last_schedule = current_schedule
			## last index is retained and not changed here
	else:	
		var current_schedule = schedule[get_current_schedule_index()]
		# print(get_current_activity_index())
		if last_schedule != current_schedule:
			print("switch schedule")
			print(get_current_schedule_index())
			last_schedule.end_schedule()
			if get_current_schedule_index() == last_index: #case chase ended
				print("resume schedule")
				current_schedule.resume_schedule()
				last_schedule = current_schedule
			else:
				current_schedule.start_schedule()
				last_schedule = current_schedule
				last_index = get_current_schedule_index()

func _animate_walking(movement_velocity: Vector2) -> void:
	var direction: Direction = Direction.vector_to_direction(movement_velocity)

	if direction.is_left():
		animation_player.play("run_left")
		vision_cone.rotation_degrees = 90
	elif direction.is_right():
		animation_player.play("run_right")
		vision_cone.rotation_degrees = -90
	elif direction.is_up():
		animation_player.play("run_back")
		vision_cone.rotation_degrees = 180
	elif direction.is_down():
		animation_player.play("run_front")
		vision_cone.rotation_degrees = 0

func animate_idle(direction: Direction = Direction.new_down()):
	if direction.is_left():
		animation_player.play("idle_left")
		vision_cone.rotation_degrees = 90
	elif direction.is_right():
		animation_player.play("idle_right")
		vision_cone.rotation_degrees = -90
	elif direction.is_up():
		animation_player.play("idle_back")
		vision_cone.rotation_degrees = 180
	elif direction.is_down():
		animation_player.play("idle_front")
		vision_cone.rotation_degrees = 0
		

func _on_area_2d_body_entered(body):  ## someone is caught
	if body == chasing:
		if body.has_method("is_player"):
			if body.is_player():
				print("caught you lacking")
				if insta_end:
					MessageBus.endgame.emit(Endgame.Reason.Catch)
				else:
					MessageBus.player_caught.emit()
