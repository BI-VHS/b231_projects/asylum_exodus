class_name PatientManager
extends Node

## Just assign managers to it and patients to be managed and rest should be automatic.
## If a time shift or something were to occur, please call special method for it
## Like sleep.

@export var schedule: PatientScheduleCollection
@export var timer_holder: Node

## PatientId -> ManagedPatient
var managed_patients: Dictionary

## ActivityId -> ActivityManager
var managers: Dictionary

var sampson_after_broom: PatientSchedule = preload("res://game/configurations/patients/individuals/sampson/sampson_after_broom_schedule.tres")

func show_quest_label(patient_id: PatientBase.PatientId):
	var managed_patient: ManagedPatient = managed_patients.get(patient_id)
	managed_patient.patient.show_quest_label()
	
func hide_quest_label(patient_id: PatientBase.PatientId):
	var managed_patient: ManagedPatient = managed_patients.get(patient_id)
	managed_patient.patient.hide_quest_label()

func _ready() -> void:
	MessageBus.sampson_got_broom.connect(sampson_got_broom)
	MessageBus.show_quest_label.connect(show_quest_label)
	MessageBus.hide_quest_label.connect(hide_quest_label)


func sampson_got_broom() -> void:
	change_patients_schedule(PatientBase.PatientId.SAMPSON, sampson_after_broom)
	var sampson: PatientBase = managed_patients.get(PatientBase.PatientId.SAMPSON).patient
	var goal = sampson.goal
	if goal is StandWithAnimationGoal:
		goal.animation_name = "sweeping"
		sampson.play_animation("sweeping")



func add_activity_manager(activity_manager: ActivityManager) -> void:
	managers[activity_manager.activity_id] = activity_manager


func remove_activity_manager_by_id(activity_id: PatientActivity.PatientActivityId) -> void:
	managers.erase(activity_id)


## Adds and activates patient, if he has a set schedule.
## Return true if operation was successfull and false if not.
func add_patient(patient: PatientBase) -> bool:
	print(patient.patient_id)
	var patient_schedule: PatientSchedule = schedule.schedule_dict.get(patient.patient_id)
	if patient_schedule == null:
		printerr("Could not add patient with id: ", patient.PatientId.get(patient.patient_id))
		return false

	_activate_patient(patient, patient_schedule)
	return true


func remove_patient(patient_id: PatientBase.PatientId) -> void:
	var managed_patient: ManagedPatient = managed_patients.get(patient_id)
	if managed_patient == null:
		return

	_deactivate_patient(managed_patient)
	managed_patients.erase(patient_id)


## In case of sampson, for it to take effect immediately it would have to be
## more complicated or we can just force the interaction outright
func change_patients_schedule(
	patient_id: PatientBase.PatientId, new_schedule: PatientSchedule
) -> void:
	var patient: ManagedPatient = managed_patients.get(patient_id)
	if patient == null:
		return

	patient.change_patients_schedule(new_schedule)


## Add patient to managed patients and start his assignments
func _activate_patient(patient: PatientBase, patient_schedule: PatientSchedule) -> void:
	var assignment_timer: Timer = Timer.new()
	timer_holder.add_child(assignment_timer)
	managed_patients[patient.patient_id] = ManagedPatient.new(
		patient, patient_schedule, assignment_timer, managers
	)


## Remove patient from managed patients and clear stop his active assignments
func _deactivate_patient(managed_patient: ManagedPatient) -> void:
	managed_patient.deactivate_assignment()
