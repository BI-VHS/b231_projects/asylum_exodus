class_name ManagedPatient
extends Node

## Responsible for managing one patient by his schedule, managing the timers
## and giving him to his managers based on his schedule and time

var patient: PatientBase
var patient_schedule: PatientSchedule
var assignment_timer: Timer

## ActivityId -> ActivityManager
## Do not change ever, property of PatientManager
var managers: Dictionary

## Sets up the timer and assigns patient to manager and remembers it
var active_assignment: ActivePatientAssignment


func _init(
	_patient: PatientBase,
	_patient_schedule: PatientSchedule,
	_assignment_timer: Timer,
	_managers: Dictionary
) -> void:
	patient = _patient
	patient_schedule = _patient_schedule
	assignment_timer = _assignment_timer
	managers = _managers
	assignment_timer.timeout.connect(_on_active_assignment_timeout)
	assignment_timer.one_shot = true
	var time: int = GlobalTime.get_minutes_since_midnight()
	var next_assignment: PatientAssignment = patient_schedule.get_assignment_by_time(time)
	_activate_new_assignment(time, next_assignment)


## Checks if current assignment should not end and starts a new one if needed.
## Usefull for time-warps and such, where timers stop being useful.
func refresh_assignment(time: int) -> void:
	if active_assignment.should_be_over(time):
		var new_assignment: PatientAssignment = _select_new_assignment_by_time(time)
		_activate_new_assignment(time, new_assignment)


func _select_new_assignment_by_time(time: int) -> PatientAssignment:
	return patient_schedule.get_assignment_by_time(time)


## A brand new assignment should always be chosen so we don't end up in
## endless timer timeout hell.
func _on_active_assignment_timeout() -> void:
	var time: int = GlobalTime.get_minutes_since_midnight()
	if time >= GlobalTime.MINUTES_IN_DAY - 1:
		time = 0
	var next_assignment: PatientAssignment
	if active_assignment.should_be_over(time):
		next_assignment = patient_schedule.get_assignment_by_time(time)
	else:  ## Timer went off too soon
		printerr("On active assignment timeout, the same assignment was selected!", patient.patient_id)
		var time_range: IntRange = active_assignment.range
		printerr("Time: ", time, ", from: ", time_range.from, ", to: ", time_range.to)
		printerr("Fallbacking on next assignment except given one!")
		next_assignment = patient_schedule.get_assignment_by_time(active_assignment.range.to)

	_activate_new_assignment(time, next_assignment)


func _activate_new_assignment(time: int, next_assignment: PatientAssignment) -> void:
	if active_assignment != null:
		active_assignment.end_assignment()

	active_assignment = ActivePatientAssignment.new(
		time, patient, next_assignment, managers, assignment_timer
	)


func deactivate_assignment() -> void:
	if active_assignment == null:
		return
	assignment_timer.stop()
	active_assignment.end_assignment()


func change_patients_schedule(new_schedule: PatientSchedule) -> void:
	patient_schedule = new_schedule
