class_name ActivePatientAssignment
extends RefCounted

## Assigns the manager and starts timer

var patient: PatientBase
var assignment: PatientAssignment
var manager: ActivityManager

var range: IntRange:
	get:
		return assignment.same_day_range


func _init(
	time: int,
	_patient: PatientBase,
	_assignment: PatientAssignment,
	_managers: Dictionary,
	timer: Timer
) -> void:
	patient = _patient
	assignment = _assignment
	manager = _pick_manager(_managers)
	_set_manager()
	_start_timer(time, timer)


func end_assignment() -> void:
	manager.remove_patient(patient)


func should_be_over(time: int) -> bool:
	return time >= assignment.range.to or time < assignment.range.from


func _pick_manager(managers: Dictionary) -> ActivityManager:
	var manager: ActivityManager = managers.get(assignment.activity.id)
	if manager == null:
		printerr("Manager for activity_id ", assignment.activity.id, " is not defined in managers!")
		assert(false)
	return manager


func _set_manager() -> void:
	manager.manage_patient(patient, assignment.activity)


func _start_timer(time: int, timer: Timer) -> void:
	timer.start(GlobalTime.how_many_real_seconds_between(time, range.to))
