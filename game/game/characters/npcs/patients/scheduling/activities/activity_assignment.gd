class_name PatientAssignment
extends Resource

## Binds an activity to a certain time range. Across time range might not be useful :D

@export var activity: PatientActivity

@export_range(0, 23) var _from_hours: int
@export_range(0, 59) var _from_minutes: int

@export_range(0, 23) var _to_hours: int
@export_range(0, 59) var _to_minutes: int

var range: IntRange:
	get:
		return same_day_range

var same_day_range: IntRange:
	get:
		if not _is_same_day_cached:
			_same_day_range_field = IntRange.new(
				_from_hours * GlobalTime.MINUTES_IN_HOUR + _from_minutes,
				_to_hours * GlobalTime.MINUTES_IN_HOUR + _to_minutes
			)
		return _same_day_range_field

var across_day_range: IntRange:
	get:
		if _is_across_day_cached:
			_across_day_range_field = _get_range_across_days()

		return _across_day_range_field

var _is_same_day_cached: bool = false
var _same_day_range_field: IntRange

var _is_across_day_cached: bool = false
var _across_day_range_field: IntRange


func is_across_days() -> bool:
	return same_day_range.from >= same_day_range.to


func _get_same_day_range() -> IntRange:
	return IntRange.new(
		_from_hours * GlobalTime.MINUTES_IN_HOUR + _from_minutes,
		_to_hours * GlobalTime.MINUTES_IN_HOUR + _to_minutes
	)


func _get_range_across_days() -> IntRange:
	if is_across_days():
		return IntRange.new(same_day_range.from, same_day_range.to + GlobalTime.MINUTES_IN_DAY)
	return same_day_range
