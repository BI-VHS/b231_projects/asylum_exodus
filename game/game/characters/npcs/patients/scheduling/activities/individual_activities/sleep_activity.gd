class_name SleepActivity
extends PatientActivity

enum BedId {
	NIGMA_BED,
	SOFTING_BED,
	SAMPSON_BED,
	SEXY_VEGETARIAN_BED,
	PA_STUDENT,
	RAINMAN,
	PUBLIC_TRANSPORT_INSPECTOR_BED,
}

@export var bed_id: BedId


func _get_activity_id() -> PatientActivityId:
	return PatientActivityId.SLEEP
