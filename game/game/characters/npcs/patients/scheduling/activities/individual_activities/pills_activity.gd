class_name PillsActivity
extends PatientActivity


func _get_activity_id() -> PatientActivityId:
	return PatientActivityId.PILLS
