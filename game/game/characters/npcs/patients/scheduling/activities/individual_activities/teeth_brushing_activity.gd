class_name TeethBrushActivity
extends PatientActivity


func _get_activity_id() -> PatientActivityId:
	return PatientActivityId.TEETH_BRUSH_ACTIVITY
