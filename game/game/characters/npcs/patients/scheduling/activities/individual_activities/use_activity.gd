class_name UseActivity
extends PatientActivity

## When patient's goal should be to just use something until the end for example
## shower, sleep, work (sit at desk) at workshop, sit at cinema.
## Note: Later a cinema/workshop manager and activity should be created,
##       which will seat the activity doers at random and tell them do
##       produce something or something like that

enum UseId {
	SOFTING_LIBRARY_CHAIR,
	SEXY_VEGETARIAN_WORKSHOP,
	SUNNYMAN_WORKSHOP,
}

@export var what: UseId


func _get_activity_id() -> PatientActivityId:
	return PatientActivityId.USE
