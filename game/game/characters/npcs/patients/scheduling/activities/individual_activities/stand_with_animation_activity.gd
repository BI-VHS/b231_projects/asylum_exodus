class_name StandWithAnimationActivity
extends PatientActivity

enum StandSpotId {
	NIGMA_TV,
	SAMPSON_COMMON_ROOM,
	PA_STUDENT_LIBRARY,
	PUBLIC_TRANSPORT_INSPECTOR_ARCHIVE,
}

@export var animation_name: String
@export var stand_spot_id: StandSpotId


func _get_activity_id() -> PatientActivityId:
	return PatientActivityId.STAND_WITH_ANIMATION
