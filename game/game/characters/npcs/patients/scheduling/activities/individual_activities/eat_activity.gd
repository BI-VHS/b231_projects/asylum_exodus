class_name EatActivity
extends PatientActivity


func _get_activity_id() -> PatientActivityId:
	return PatientActivityId.EAT
