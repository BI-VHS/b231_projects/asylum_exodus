class_name PatientActivity
extends Resource

## Defines a patient's activity and accompanying individual configuration.

enum PatientActivityId {
	USE,
	SLEEP,
	EAT,
	TEETH_BRUSH_ACTIVITY,
	CINEMA,  ## Not implemented
	PILLS,
	STAND_WITH_ANIMATION,
}

var id: PatientActivityId:
	get:
		return _get_activity_id()


## Each extending class should override this!
func _get_activity_id() -> PatientActivityId:
	assert(false, "PatientActivity base class does not posses an ID!")
	return PatientActivityId.values()[0]
