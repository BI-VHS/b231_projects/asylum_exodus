class_name PatientScheduleCollection
extends Resource

## Defines daily schedule of all patients

@export var schedules: Array[PatientSchedule] = []

## PatientId -> PatientSchedule
var schedule_dict: Dictionary:
	get:
		if not _is_schedule_dict_cached:
			_schedule_dict_field = _create_dict()

		return _schedule_dict_field

var _is_schedule_dict_cached: bool = false
var _schedule_dict_field: Dictionary = {}


func _create_dict() -> Dictionary:
	var res: Dictionary
	for schedule in schedules:
		print("adding ", schedule.patient_id)
		res[schedule.patient_id] = schedule
		print(res[schedule.patient_id])
	return res
