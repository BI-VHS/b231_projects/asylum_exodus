class_name PatientSchedule
extends Resource

## Defines one patient's daily schedule. Please make the assignments properly
## linked (e.g. 00:00 - 2:00, 2:00 - ...) and don't go over midnight:
## 22:00 - 2:00 is not allowed, please chunk it accordingly

@export var patient_id: PatientBase.PatientId

@export var _assignments: Array[PatientAssignment] = []


## Given a time since midnight, returns assignment with range containing that time
func get_assignment_by_time(time: int) -> PatientAssignment:
	if time == 1439: ## 23:59 EDGE CASE
		time = 1438
	var return_assignment: PatientAssignment
	for assignment: PatientAssignment in _assignments:
		if assignment.same_day_range.contains_left_inclusive(time):
			return assignment

	assert(false, "Malformed schedule, no assignment in given time was found!")
	return null
