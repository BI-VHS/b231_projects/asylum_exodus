class_name EatPillsGoal
extends ConsumptionGoal

var sitting_spot: Usable


func _init(_buffer: ProductionBuffer, _sitting_spot: Usable) -> void:
	super._init(_buffer)
	sitting_spot = _sitting_spot
