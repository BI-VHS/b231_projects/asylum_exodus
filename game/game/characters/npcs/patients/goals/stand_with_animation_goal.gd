class_name StandWithAnimationGoal
extends Goal

var animation_name: String
var animation_spot: Vector2


func _init(_animation_name: String, _animation_spot: Vector2) -> void:
	animation_name = _animation_name
	animation_spot = _animation_spot
