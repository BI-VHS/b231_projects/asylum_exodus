class_name Goal
extends RefCounted

## Base class for data classes describing a patient's goal.
