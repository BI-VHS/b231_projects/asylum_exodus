class_name ConsumptionGoal
extends Goal

signal consumption_spot_reserved

var consumption_position: Vector2 = Vector2.INF
var reserved_index: int = -1
var _buffer: ProductionBuffer


func _init(buffer: ProductionBuffer) -> void:
	_buffer = buffer


func position_reserved(index: int) -> void:
	reserved_index = index
	consumption_position = _buffer.item_consumer_positions[index]
	consumption_spot_reserved.emit()


## Callable only once
func get_product() -> Sprite2D:
	return _buffer.pop(reserved_index)
