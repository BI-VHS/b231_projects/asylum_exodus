class_name EatFoodGoal
extends ConsumptionGoal

var food: Food
var sitting_spot: EatingSpot


func _init(_buffer: ProductionBuffer, _sitting_spot: EatingSpot) -> void:
	super._init(_buffer)
	sitting_spot = _sitting_spot
