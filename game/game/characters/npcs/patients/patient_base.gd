class_name PatientBase
extends BaseNpc

## Transitions from one state to the same state work in that it exits and re-enters
## TODO: Maybe it really would be the best if the manager set patients goal to use some chair
## and then picked random guy to change his state to GetConsumption and then signal
## consumption gotten and send him back to his chair with the food in another goal which
## would be EatFood and tere would be the vomiting and stuff

## So there will be a split between consumption and eating food
## and managers will manage what the patient will do before/after the consumption
## and we will add signals to make it simple

enum PatientId {
	NIGMA,
	SOFTING,
	SAMPSON,
	PA_STUDENT,
	RAINMAN,
	PUBLIC_TRANSPORT_INSPECTOR,
	SEXY_VEGETARIAN,
}

@export var dialogue_id: DialogueDB.DialogueId
@export var patient_id: PatientId

@export var _goto_eat_speech: Array[String] = ["Je čas jít na oběd", "Mizím na oběd...", "Čas na jídlo", "Jde se jíst"]
@export var _goto_pills_speech: Array[String] = ["Jdu si pro léky", "Musím si jít pro další dávku léčiv", "Zase léky?"]
#@export var _on_sit_down_speech: Array[String] = ["Jak že se to vlastně sedí?", "Tohle mi nějak nejde...", "Rád bych si sedl, ale..."]
#@export var _on_eat_speech: Array[String] = ['Fakt "mňamka"', "Moje oblíbené jídlo!", "*mlask*"]
@export var _goto_sleep_speech: Array[String] = ["Jdu na kutě", "Jdu spinkat", "A večerníček nebude?"]
@export var _on_get_in_bed_speech: Array[String] = ["Dobrou noc", "Hezký sny", "Postýlka!"]
@export var _goto_product_speech: Array[String] = ["Jsem na řadě", "Teď já", "Jdu si pro to"]
@export var _on_food_picked_up_speech: Array[String] = ["Já ale chtěl něco jiného...", "No, mohlo to být i horší...", "Ooo, moje oblíbené jídlo!"]


@export var _state_chart: StateChart
@export var _animated_sprite: AnimatedSprite2D

const VOMIT = preload("res://game/items/animated/vomit.tscn")

@onready var quest_label: Label = $QuestLabel

func show_quest_label():
	quest_label.show()
	
func hide_quest_label():
	quest_label.hide()

var goal: Goal

#region Goals
var _stand_with_animation_goal: StandWithAnimationGoal:
	get:
		if not goal is StandWithAnimationGoal:
			assert(
				false, "Patient was expecting goal to be StandWithAnimationGoal, but it was not!"
			)
		return goal

var _eat_food_goal: EatFoodGoal:
	get:
		if not goal is EatFoodGoal:
			assert(false, "Patient was expecting goal to be EatFoodGoal, but it was not!")
		return goal

var _eat_pills_goal: EatPillsGoal:
	get:
		if not goal is EatPillsGoal:
			assert(false, "Patient was expecting goal to be EatPillsGoal, but it was not!")
		return goal

var _use_goal: UseGoal:
	get:
		if not goal is UseGoal:
			assert(false, "Patient was expecting goal to be UseGoal, but it was not!")
		return goal

var _consumption_goal: ConsumptionGoal:
	get:
		if not goal is ConsumptionGoal:
			assert(false, "Patient was expecting goal to be ConsumptionGoal, but it was not!")
		return goal
#endregion

#region Animating Activity Data
var _animating_animation_name: String
#endregion
#region Walking Activity Data
var _walking_destination: Vector2
#endregion
#region Using Activity Data
var _using_usable: Usable
var _old_usable: Usable
#endregion

var _last_sent_event: String = ""

@onready var eat_timer: Timer = $EatTimer
@onready var vomit_timer: Timer = $VomitTimer


func _ready() -> void:
	_animated_sprite.play()
	_state_chart.send_event("head_empty_goal_set")


func play_animation(animation_name: String) -> void:
	animation_player.play(animation_name)


## Using _send_event, which deffers the call of _state_chart.send_event, because
## it was cuasing truble, if called in _ready() function
func change_goal(new_goal: Goal) -> void:
	goal = new_goal
	if new_goal is StandWithAnimationGoal:
		_send_event("stand_with_animation_goal_set")
	elif new_goal is EatFoodGoal:
		show_speech(_goto_eat_speech.pick_random())
		_send_event("eat_food_goal_set")
	elif new_goal is EatPillsGoal:
		show_speech(_goto_pills_speech.pick_random())
		_send_event("eat_pills_goal_set")
	elif new_goal is UseGoal:
		_send_event("use_goal_set")
		_last_sent_event = "use_goal_set"
	else:
		printerr("Goal was not recognized by by patient, fallbacking to head empty!")
		_state_chart.send_event("head_empty_goal_set")


func _send_event(event: String) -> void:
	_state_chart.send_event.call_deferred(event)


#region StandWithAnimationGoal
func _on_go_to_position_state_entered() -> void:
	_start_walking(_stand_with_animation_goal.animation_spot)


func _on_animate_state_entered() -> void:
	_start_animating(_stand_with_animation_goal.animation_name)


#endregion
#region UseGoal
func _on_use_state_entered() -> void:
	_start_using(_use_goal.what)


#endregion
#region ConsumeGoal
## Used for both EatPills and EatFood
func _on_goto_product_state_entered() -> void:
	show_speech(_goto_product_speech.pick_random())
	_start_walking(_consumption_goal.consumption_position)


func _on_consumption_spot_reserved() -> void:
	print("Consumption spot reserved callback!")
	_state_chart.send_event("consumption_spot_reserved")


func _on_goto_product_state_exited() -> void:
	var product = _consumption_goal.get_product()
	if product is Food and goal is EatFoodGoal:
		show_speech(_on_food_picked_up_speech.pick_random())
		_eat_food_goal.food = product
	else:
		product.queue_free()
	_state_chart.send_event("product_gotten")


#endregion
#region EatPills
func _on_wait_for_pills_state_entered() -> void:
	print("Eat pills started")
	_start_using(_eat_pills_goal.sitting_spot)
	_eat_pills_goal.consumption_spot_reserved.connect(_on_consumption_spot_reserved)


func _on_go_back_state_entered() -> void:
	_on_goto_product_state_exited()
	_start_using(_eat_pills_goal.sitting_spot)


#endregion
#region EatFoodGoal
func _on_wait_for_food_state_entered() -> void:
	_start_using(_eat_food_goal.sitting_spot)
	_eat_food_goal.consumption_spot_reserved.connect(_on_consumption_spot_reserved)


func _on_sit_with_food_state_entered() -> void:
	_on_goto_product_state_exited()
	_start_using(_eat_food_goal.sitting_spot)


func _on_eat_food_state_entered() -> void:
	eat_timer.start()


func _on_eat_timer_timeout() -> void:
	print("Eat timer timeout, food poisoned: ", _eat_food_goal.food.is_poisoned)
	if _eat_food_goal.food.is_poisoned:
		_state_chart.send_event("food_was_spiked")
	else:
		_state_chart.send_event("food_was_ok")


func _on_vomit_state_entered() -> void:
	show_speech("Je mi nějak zle, asi budu...")
	printerr("patient started vomitting")
	var new_vomit = VOMIT.instantiate()
	get_tree().current_scene.add_child(new_vomit)
	new_vomit.global_position = global_position
	MessageBus.vomit_created.emit(new_vomit)
	_start_animating("idle")  ## TODO: When vomiting animation is done, replace
	if patient_id == PatientId.SEXY_VEGETARIAN:
		MessageBus.condition_done.emit(QuestHandler.QuestID.QUEST1, "poison")
		MessageBus.mark_checked.emit(3)
		MessageBus.expose_condition.emit(4)
		MessageBus.create_popup.emit("Podmínka splněna", "Otrav oběd Sexy Vegetariánovi")
		#MessageBus.create_popup.emit("Nový Úkol", "Promluv si s Ericem")
		print("Sexy V vomitted")
	vomit_timer.start()


func _on_vomit_timer_timeout() -> void:
	show_speech("Už je mi líp")
	_state_chart.send_event("vomiting_stopped")
	printerr("Patient vomited")


func _on_sit_after_state_entered() -> void:
	_start_using(_eat_food_goal.sitting_spot)


func _on_eat_food_state_exited() -> void:
	vomit_timer.stop()
	eat_timer.stop()


#endregion
#region AnimatingActivity
func _start_animating(animation_name: String) -> void:
	_animating_animation_name = animation_name
	_state_chart.send_event("start_animating")


func _on_animating_state_entered() -> void:
	animation_player.play(_animating_animation_name)


func _on_animating_state_exited() -> void:
	_animating_animation_name = ""


#endregion
#region StandingActivity
func _start_standing() -> void:
	_state_chart.send_event("start_standing")


func _on_standing_state_entered() -> void:
	animation_player.play("idle")


#endregion
#region WalkingActivity
func _start_walking(where: Vector2) -> void:
	_walking_destination = where
	_state_chart.send_event("start_walking")


func _on_walking_state_physics_processing(delta: float) -> void:
	navigate_to_position(_walking_destination, delta)


func _on_navigation_agent_2d_navigation_finished() -> void:
	_state_chart.send_event("navigation_finished")


func _on_walking_state_exited() -> void:
	_walking_destination = Vector2.INF


#endregion
#region UsingActivity
func _start_using(what: Usable) -> void:
	_using_usable = what
	_state_chart.send_event("start_using")
	if what is Bed and _using_usable != _old_usable:
		show_speech(_goto_sleep_speech.pick_random())



func _on_using_state_entered() -> void:
	printerr("Started using")
	_using_usable.use(self)
	_state_chart.send_event("started_using")
	_old_usable = _using_usable
	if _using_usable is Bed:
		show_speech(_on_get_in_bed_speech.pick_random())


func _on_get_to_usable_state_physics_processing(delta: float) -> void:
	navigate_to_position(_using_usable.global_position, delta)
	if not Engine.get_physics_frames() % 15 == 0:  ## every 15 frames
		return
	if global_position.distance_to(_using_usable.global_position) <= _using_usable.use_distance:
		_state_chart.send_event("usable_in_reach")


func _on_using_state_exited() -> void:
	#print("Before stopping to use usable!", patient_id)
	_old_usable.stop_using()
	#print("Setting usable to null", patient_id)
	_old_usable = null


#endregion


## Head Empty
func _on_head_empty_state_entered() -> void:
	#if _last_sent_event != "head_empty_goal_set":
	#	_state_chart.send_event(_last_sent_event)
	#	return
	print("Head empty")
	#goal = null
