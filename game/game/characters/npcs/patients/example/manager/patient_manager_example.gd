extends Node2D

var patients: Array[PatientBase]
var activity_managers: Array[ActivityManager]

@onready var patient_manager: PatientManager = $PatientManager
@onready var patient_holder: Node = $Patients
@onready var activity_manager_holder: Node = $ActivityManagers
@onready var time_info: Label = %TimeInfo


func _ready() -> void:
	GlobalTime.one_minute_duration = 5 / 100

	for patient: PatientBase in patient_holder.get_children():
		patients.push_back(patient)

	for activity_manager: ActivityManager in activity_manager_holder.get_children():
		activity_managers.push_back(activity_manager)

	for activity_manager: ActivityManager in activity_managers:
		patient_manager.add_activity_manager(activity_manager)

	for patient: PatientBase in patients:
		patient_manager.add_patient(patient)


var once: bool = true


func _physics_process(_delta: float) -> void:
	if once:
		once = false
		GlobalTime.one_minute_duration = 5 / 100
	time_info.text = "Clock: " + GlobalTime.time_as_string(GlobalTime.get_minutes_since_midnight())
