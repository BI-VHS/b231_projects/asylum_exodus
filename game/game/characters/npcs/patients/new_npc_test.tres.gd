@tool
class_name NPCNewTest
extends BaseNpc

## Class of ProducentNPCs, they have a workstation where they work and a place
## or rather a buffer where they put down items they produced
## They need a valid Production buffer, workstation coordinates, time to wait
## at workstation, an array of sprites they can generate and character sprite
## They also need all other settings inherited from BaseNPC
