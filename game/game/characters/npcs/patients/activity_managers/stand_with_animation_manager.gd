class_name StandWithAnimationActivityManager
extends ActivityManager

## As with using acitivity manger, we have to pair it
@export var _stand_spots_ids: Array[StandWithAnimationActivity.StandSpotId]
@export var _stand_spots: Array[Node2D]

## StandWithAnimationActivity.StandSpotId -> Vector2
var stand_spot_id_to_position: Dictionary


func _ready() -> void:
	var index = 0
	while index < _stand_spots_ids.size():
		stand_spot_id_to_position[_stand_spots_ids[index]] = _stand_spots[index].global_position
		index += 1


func manage_patient(patient: PatientBase, _activity: PatientActivity) -> void:
	super.manage_patient(patient, _activity)
	if _activity is StandWithAnimationActivity:
		patient.change_goal(
			StandWithAnimationGoal.new(
				_activity.animation_name, stand_spot_id_to_position[_activity.stand_spot_id]
			)
		)
	else:
		assert(false, "StandActivityManager received different activity than StandACtivity!")


func remove_patient(patient: PatientBase) -> void:
	super.remove_patient(patient)
