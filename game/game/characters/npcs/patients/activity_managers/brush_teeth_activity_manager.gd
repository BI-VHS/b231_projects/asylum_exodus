class_name BrushTeethActivityManager
extends ActivityManager

@export var sink_spots: Array[Node2D]
@export var teeth_brushing_animation_name: String
## True if sink is free
var _free_sink_positions: Array[Vector2]


func _ready() -> void:
	for spot in sink_spots:
		_free_sink_positions.push_back(spot.global_position)


func manage_patient(patient: PatientBase, _activity: PatientActivity) -> void:
	super.manage_patient(patient, _activity)
	var random_free_sink_spot: Vector2 = _get_random_free_sink()
	patient.change_goal(
		StandWithAnimationGoal.new(teeth_brushing_animation_name, random_free_sink_spot)
	)


func remove_patient(patient: PatientBase) -> void:
	super.remove_patient(patient)
	_free_sink_positions.push_back(patient._stand_with_animation_goal.animation_spot)


func _get_random_free_sink() -> Vector2:
	var sink_spot: Vector2 = _free_sink_positions.pick_random()
	_free_sink_positions.erase(sink_spot)
	return sink_spot
