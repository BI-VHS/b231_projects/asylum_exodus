class_name SleepActivityManager
extends ActivityManager

@export var beds: Array[Bed]
## PatientId -> Bed
var patient_to_bed: Dictionary


func _ready() -> void:
	activity_id = PatientActivity.PatientActivityId.SLEEP
	for bed in beds:
		patient_to_bed[bed.owner_id] = bed


func manage_patient(patient: PatientBase, _activity: PatientActivity) -> void:
	super.manage_patient(patient, _activity)
	print("Adding sleep acitivity patient")
	patient.change_goal(UseGoal.new(patient_to_bed[patient.patient_id]))


func remove_patient(patient: PatientBase) -> void:
	super.remove_patient(patient)
