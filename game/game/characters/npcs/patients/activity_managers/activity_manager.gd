class_name ActivityManager
extends Node

## Holds all information necessary to order patients to do given activity

@export var activity_id: PatientActivity.PatientActivityId

## PatientId -> Patient
var _managed_patients: Dictionary


func manage_patient(patient: PatientBase, _activity: PatientActivity) -> void:
	if _activity.id != activity_id:
		printerr("Activity manager receive activity, with different id than manager's!")
	_managed_patients[patient.patient_id] = patient


func remove_patient(patient: PatientBase) -> void:
	_managed_patients.erase(patient.patient_id)
