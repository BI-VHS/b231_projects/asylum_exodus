class_name ConsumptionActivityManager
extends ActivityManager

## Keeps waiting patients and sends them for consumption when necessary

@export var buffer: ProductionBuffer
@export var retry_timer: Timer

var _waiting_patients: Array[PatientBase]


func _ready() -> void:
	retry_timer.timeout.connect(_on_retry_timer_timeout)


func manage_patient(patient: PatientBase, _activity: PatientActivity) -> void:
	super.manage_patient(patient, _activity)
	_waiting_patients.push_back(patient)
	print("Before changing patient's goal")
	patient.change_goal(_create_goal(patient))
	if retry_timer.is_stopped():
		retry_timer.start()


func remove_patient(patient: PatientBase) -> void:
	super.remove_patient(patient)
	_on_remove_patient(patient)
	if _managed_patients.size() <= 0:
		retry_timer.stop()


## Please override in subclasses
func _create_goal(_patient: PatientBase) -> Goal:
	assert(false, "Should be always overriden")
	return Goal.new()


## What to do when removing a patient
func _on_remove_patient(_patient: PatientBase) -> void:
	pass


func _on_retry_timer_timeout() -> void:
	if _waiting_patients.size() <= 0:
		retry_timer.stop()
		return

	var full_index: int
	while _waiting_patients.size() > 0:
		full_index = buffer.return_full_index()
		if full_index == -1:
			return

		buffer.reserve_index_consumption(full_index)
		_send_random_patient_to_index(full_index)


## Sends patient and notifies the goal
func _send_random_patient_to_index(full_index: int) -> void:
	var patient_to_send: PatientBase = _waiting_patients.pick_random()
	_waiting_patients.erase(patient_to_send)

	var goal: ConsumptionGoal = patient_to_send.goal
	goal.position_reserved(full_index)
