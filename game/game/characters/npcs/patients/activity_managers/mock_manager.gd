class_name MockManager
extends ActivityManager

## Simply prints on top of basic ActivityManager


func manage_patient(patient: PatientBase, _activity: PatientActivity) -> void:
	print(print_activity_id(), ", adding patient ", patient_id_to_str(patient.patient_id))
	print_activity_id()
	super.manage_patient(patient, _activity)


func remove_patient(patient: PatientBase) -> void:
	print(print_activity_id(), ", removing patient ", patient_id_to_str(patient.patient_id))
	super.remove_patient(patient)


func print_activity_id() -> String:
	return (
		"In manager of: "
		+ EnumPrintHelpers.name_by_id(activity_id, PatientActivity.PatientActivityId)
	)


func patient_id_to_str(patient_id: PatientBase.PatientId) -> String:
	for str: String in PatientBase.PatientId:
		if PatientBase.PatientId[str] == patient_id:
			return str
	assert(false, "Bad Invaild patientId given to MockManager!")
	return ""
