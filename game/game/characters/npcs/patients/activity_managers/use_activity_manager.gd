class_name UseActivityManager
extends ActivityManager

## Note: Don't have a way how to do it in type-safe way atm, gotta go fast
##       would love to do this differently, but oh well
@export var usable_nodes: Array[Usable]
@export var usable_ids: Array[UseActivity.UseId]
## UseActivity.UseId -> Usable nodes
var usables_as_dict: Dictionary


func _ready() -> void:
	var index = 0
	while index < usable_nodes.size():
		usables_as_dict[usable_ids[index]] = usable_nodes[index]
		index += 1


func manage_patient(patient: PatientBase, activity: PatientActivity) -> void:
	if activity is UseActivity:
		super.manage_patient(patient, activity)
		patient.change_goal(UseGoal.new(usables_as_dict[activity.what]))
	else:
		assert(false, "UseActivityManager received non-use activity!")


func remove_patient(patient: PatientBase) -> void:
	super.remove_patient(patient)


class UseIdToUsable:
	extends Node
	@export var usable: Usable
	@export var use_id: UseActivity.UseId
