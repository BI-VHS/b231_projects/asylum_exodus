class_name EatActivityManager
extends ConsumptionActivityManager

@export var _sitting_spots: Array[EatingSpot]

var _free_sitting_spots: Array[EatingSpot]


func _ready() -> void:
	retry_timer.timeout.connect(_on_retry_timer_timeout)
	_free_sitting_spots = _sitting_spots.duplicate()


func _create_goal(_patient: PatientBase) -> EatFoodGoal:
	return EatFoodGoal.new(buffer, _get_free_spot_to_sit())


func _on_remove_patient(patient: PatientBase) -> void:
	var goal: EatFoodGoal = patient.goal
	_free_sitting_spots.push_back(goal.sitting_spot)


func _get_free_spot_to_sit() -> EatingSpot:
	var spot: EatingSpot = _free_sitting_spots.pick_random()
	_free_sitting_spots.erase(spot)
	return spot
