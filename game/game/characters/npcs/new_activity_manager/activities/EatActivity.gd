@tool
class_name eatActivity
extends BaseActivity

@export var chair: Node2D
var buffer: ProductionBuffer
@onready var nav_agent = activity_manager.npc.navigation_agent
@onready var eat_timer = $EatTimer
@onready var vomit = $Vomit
var index
var food

func _ready():
	buffer = get_parent().get_parent().buffer

func _on_wait_for_food_state_physics_processing(delta):
	buffer = get_parent().get_parent().buffer
	activity_manager.npc.navigate_to_position(chair.global_position, delta)
	index = buffer.return_full_index()
	if index != -1:
		buffer.reserve_index_consumption(index)
		print("taking food")
		state_chart.send_event("take_food")
	
	if nav_agent.is_navigation_finished():
		activity_manager.npc.animation_player.play("idle")


func _on_take_food_state_physics_processing(delta):
	activity_manager.npc.navigate_to_position(buffer.item_consumer_positions[index], delta)
	if nav_agent.is_navigation_finished():
		food = buffer.pop(index)
		index = -1
		print("going to sit down")
		state_chart.send_event("sit")


func _on_sit_down_state_physics_processing(delta):
	activity_manager.npc.navigate_to_position(chair.global_position, delta)
	if nav_agent.is_navigation_finished():
		activity_manager.npc.animation_player.play("idle")
		eat_timer.start()
		# await get_tree().create_timer(1000).timeout
		print("going to eat")
		state_chart.send_event("eat")


func _on_eat_state_physics_processing(delta):
	pass


func _on_vomit_state_entered():
	var new_vomit = vomit.duplicate()
	new_vomit.position = activity_manager.npc.position
	MessageBus.vomit_created.emit(new_vomit)
	print("going to stand up")
	state_chart.send_event("stand_up")


func _on_eat_state_entered():
	await eat_timer.timeout
	if food.is_poisoned:
		food.queue_free()
		print("going to vomit")
		state_chart.send_event("vomit")
	food.queue_free()
	print("going to stand up")
	state_chart.send_event("stand_up")
