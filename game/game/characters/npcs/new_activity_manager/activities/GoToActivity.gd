@tool
class_name GoToActivity
extends BaseActivity

@export var go_to_position: Vector2
@onready var nav_agent = activity_manager.npc.navigation_agent

func _on_go_to_state_physics_processing(delta):
	activity_manager.npc.navigate_to_position(go_to_position, delta)
	if nav_agent.is_navigation_finished():
		print("nav finished")
		state_chart.send_event("navigation_finished")
	
func _on_stand_state_entered():
	activity_manager.npc.animation_player.play("idle")
