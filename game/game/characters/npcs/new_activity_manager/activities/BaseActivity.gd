@tool
class_name  BaseActivity
extends Node

@export var activity_manager: ActivityManagerNew
@export var state_chart: StateChart
@export var from: Vector2
@export var to: Vector2

func start_activity():
	state_chart.send_event("start")
	
func end_activity():
	state_chart.send_event("end")
