@tool
class_name ActivityManagerNew
extends Node

@export var npc: BaseNpc
@export var sprite: AnimatedSprite2D
@export var activities: Array[BaseActivity]
var current_time: Vector2
var kickstart = true
# var time_keeper
var last_activity: BaseActivity
var last_index

func get_current_activity_index() -> int:
	var x: int = 0
	while x < activities.size():
		var act = activities[x]
		if current_time[0] >= act.from[0] and current_time[0] <= act.to[0]:
			if current_time[1] >= act.from[1] and current_time[1] <= act.to[1]:
				return x
		x += 1
	return last_index

# Called when the node enters the scene tree for the first time.
func _ready():
	# time_keeper = get_node("/root/GlobalTime")
	GlobalTime.update.connect(time_check)
	sprite.play("idle")

func time_check(new_time: Vector2):
	current_time = new_time
	if kickstart:
		var current_activity = activities[get_current_activity_index()]
		last_index = get_current_activity_index()
		last_activity = current_activity
		current_activity.start_activity()
		kickstart = false
	
	var current_activity = activities[get_current_activity_index()]
	# print(get_current_activity_index())
	if last_activity != current_activity:
		print("switch activity")
		print(get_current_activity_index())
		last_activity.end_activity()
		current_activity.start_activity()
		last_activity = current_activity
		last_index = get_current_activity_index()


