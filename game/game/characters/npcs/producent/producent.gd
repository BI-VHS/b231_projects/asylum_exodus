@tool
class_name ProducentNPC
extends BaseNpc

## Class of ProducentNPCs, they have a workstation where they work and a place
## or rather a buffer where they put down items they produced
## They need a valid Production buffer, workstation coordinates, time to wait
## at workstation, an array of sprites they can generate and character sprite
## They also need all other settings inherited from BaseNPC

var workstation_position: Vector2  ## Position of Producents workstation
@export var workstation_position_node: Node2D
@export var production_buffer: ProductionBuffer  ## Production Buffer where producent will add items
@export var workstation_time: float  ## wait time at workstation before going back to buffer
@export var generate_item_array: Array[Sprite2D]  ## array of items producent can generate
@export var animated_sprite: AnimatedSprite2D  ## character sprite
@export var zonemng: ProhibitedZoneManager
var go_to_index: int  ## index in buffer where producent will go to
var rng = RandomNumberGenerator.new()  ## random number generator

@onready var state_chart: StateChart = $StateChart
@onready var work_timer: Timer = $WorkTimer
@onready var retry_timer: Timer = $RetryTimer


func _get_configuration_warnings():
	return super._get_configuration_warnings()


## assings set workstation wait time to internal timer
func _ready():
	workstation_position = workstation_position_node.global_position
	work_timer.wait_time = workstation_time


## stand task
## works for a while, then tries to go to production buffer if a slot is available
## otherwise waits a second and tries again
func _on_stand_task_state_entered():
	animation_player.play("idle")
	# print(position)
	# print(workstation_position)
	work_timer.start()
	await work_timer.timeout

	go_to_index = production_buffer.return_empty_index()
	while go_to_index == -1:
		print("Producent: No empty slot")
		retry_timer.start()
		await retry_timer.timeout
		go_to_index = production_buffer.return_empty_index()
	print("Producent: Finished working, going to add item")

	production_buffer.reserve_index_production(go_to_index)
	state_chart.send_event("take_item")


## going to production buffer
func _on_take_item_task_state_physics_processing(delta):
	navigate_to_position(production_buffer.item_producer_positions[go_to_index], delta)


## generates an item to leave in the production buffer
func _on_take_item_task_state_exited():
	var sprite = generate_item_array[rng.randi_range(0, generate_item_array.size() - 1)].duplicate()
	sprite.position = Vector2(0, 0)
	production_buffer.insert(sprite, go_to_index)
	print("Producent: Added new item to buffer, going back")


## send event when finished walking
func _on_navigation_agent_2d_navigation_finished():
	state_chart.send_event("navigation_finished")


## going to workstation
func _on_go_back_task_state_physics_processing(delta):
	navigate_to_position(workstation_position, delta)


func _on_compound_state_state_entered():
	animated_sprite.play("idle")
	# print(workstation_position)
	# print(position)
	
var closest_guard: BaseGuard = null

func get_nearest_guard() -> BaseGuard:
	var guards = get_tree().get_nodes_in_group("guards")
	if guards.is_empty():
		return null
	var closest: BaseGuard = guards[0]
	for g: BaseGuard in guards:
		if global_position.distance_to(closest.global_position) > global_position.distance_to(g.global_position):
			closest = g
	return closest

func _on_vision_cone_2d_vision_cone_body_entered(body: Node2D):
	if body.has_method("is_player"):
		if zonemng.is_body_in_prohibited_zone(body):
			if closest_guard == null:
				closest_guard = get_nearest_guard()
			closest_guard.tresspassing_player_override = body
			speech_label.text = "Stráže!"


func _on_vision_cone_2d_vision_cone_body_exited(body):
	if body.has_method("is_player"):
		if closest_guard != null:
			closest_guard.tresspassing_player_override = null
			closest_guard = null
			speech_label.text = ""

