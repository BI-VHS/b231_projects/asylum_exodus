@tool
class_name JanitorNPC
extends BaseNpc

## Script for JanitorNPC, please specify the time it takes to clean vomit and
## where Janitor resides when not working
## he has a queue of vomits he needs to clean
## he qoes to them one by one and cleans them, if there is no vomit to clean
## he heads back to his wait position

@export var wait_position: Vector2 ## place where janitor hangs out by default
@export var cleaning_time: float ## time it takes to clean vomit
var vomit_queue: Array[StaticBody2D] ## use as a queue for vomit to clean
var current_vomit: StaticBody2D ## current vomit that janitor is navigating to
var root: Node ## root scene node, to assign vomit as child to
@export var zonemng: ProhibitedZoneManager

@onready var state_chart: StateChart = $StateChart
@onready var clean_timer: Timer = $CleanTimer
@onready var animated_sprite: AnimatedSprite2D = $AnimatedSprite2D


## does not work for unknown reason
func is_in_room() -> bool:
	return zonemng.get_zone_containing_body(self).zone_id == Zone.ZoneId.GF_JANITOR_ROOM


## init variables
func _ready():
	MessageBus.vomit_created.connect(insert_vomit)
	clean_timer.wait_time = cleaning_time
	root = get_tree().current_scene
	
#func _process(delta):
#	if QuestHandler.current_quest == 2:
#		if QuestHandler.quest2["lure_out"] == false:
#			if not is_in_room():
#				MessageBus.condition_done.emit(QuestHandler.QuestID.QUEST2, "lure_out")
#				MessageBus.mark_checked.emit(1)
#				MessageBus.create_popup.emit("Podmínka splněna", "Zaměstnej uklízeče")

## inserts new vomit to queue and adopts it
func insert_vomit(vomit: StaticBody2D):
	vomit_queue.push_back(vomit)
	root.add_child(vomit)
	
## pops the vomit at the front, vomit is still a child
func pop_vomit():
	current_vomit = vomit_queue.pop_front()

## debug
func _on_wait_task_state_entered():
	print("Waiting for a new vomit to clean")
	animation_player.play("idle")

## waiting for a vomit to clean
func _on_wait_task_state_physics_processing(delta):
	if vomit_queue.is_empty() == false:
		pop_vomit()
		print("new vomit found")
		state_chart.send_event("new_vomit")
		
## going to first vomit
func _on_go_to_task_state_physics_processing(delta):
	navigate_to_position(current_vomit.position, delta)

# add cleaning animation
## cleaning the vomit, waits and deletes the vomit
func _on_cleaning_task_state_entered():
	animation_player.play("clean")
	clean_timer.start()
	await clean_timer.timeout
	root.remove_child(current_vomit)
	current_vomit.queue_free()
	print("cleaned vomit")
	state_chart.send_event("cleaned_vomit")

## navigation finish
func _on_navigation_agent_2d_navigation_finished():
	state_chart.send_event("navigation_finished")

## going back to wait position, interrupts it if a new vomit is created
func _on_go_back_task_state_physics_processing(delta):
	if vomit_queue.is_empty() == false:
		pop_vomit()
		print("new vomit found")
		state_chart.send_event("new_vomit")
	navigate_to_position(wait_position, delta)

func _on_compound_state_state_entered():
	animated_sprite.play("idle")
	

var closest_guard: BaseGuard = null

func get_nearest_guard() -> BaseGuard:
	var guards = get_tree().get_nodes_in_group("guards")
	if guards.is_empty():
		return null
	var closest: BaseGuard = guards[0]
	for g: BaseGuard in guards:
		if global_position.distance_to(closest.global_position) > global_position.distance_to(g.global_position):
			closest = g
	return closest

func _on_vision_cone_2d_vision_cone_body_entered(body: Node2D):
	if body.has_method("is_player"):
		if zonemng.is_body_in_prohibited_zone(body):
			if closest_guard == null:
				closest_guard = get_nearest_guard()
			closest_guard.tresspassing_player_override = body
			speech_label.text = "Stráže!"


func _on_vision_cone_2d_vision_cone_body_exited(body):
	if body.has_method("is_player"):
		if closest_guard != null:
			closest_guard.tresspassing_player_override = null
			closest_guard = null
			speech_label.text = ""
