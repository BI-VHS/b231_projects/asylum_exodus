extends Node2D

@export var vomit: StaticBody2D
@onready var player: Player = $PlayerCamera

## spawns a new vomit on "V" keypress and sends the signal
func _input(event):
	if event is InputEventKey and event.is_released():
		if event.get_key_label() == 86 || event.get_key_label() == 118:
			var new_vomit = vomit.duplicate()
			new_vomit.position = player.position
			MessageBus.vomit_created.emit(new_vomit)
			
	
