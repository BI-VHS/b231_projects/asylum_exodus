class_name SittingSpot
extends Usable

@export var direction: Direction
@export var sit_position: Vector2
## Where the user should be dropped-off after end of using
@export var get_off_spot: Node2D
