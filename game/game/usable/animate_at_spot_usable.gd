class_name AnimateInPlaceUsable
extends Usable

@export var animation_name: String
@export var _animation_position_node: Node2D
@export var _get_out_position_node: Node2D

var animation_position: Vector2:
	get:
		return _animation_position_node.global_position
var get_out_position: Vector2:
	get:
		return _get_out_position_node.global_position


func use(user: Node2D) -> void:
	if user is Player:
		user.play_animation(animation_name)
	super.use(user)
