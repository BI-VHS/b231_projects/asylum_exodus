class_name Usable
extends Node

@export var use_distance: float

var user: Node2D


func is_used() -> bool:
	return user != null


func use(_user: Node2D) -> void:
	if is_used():
		printerr("use() was called, but there was a user already!")
	user = _user


func stop_using() -> void:
	if not is_used():
		printerr("stop_using() was called, when there was no user")

	user = null
