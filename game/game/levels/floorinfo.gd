extends Label

@onready var player: Player = get_tree().get_nodes_in_group("player")[0]
const text_ground: String = "Přízemí"
const text_first: String = "První Patro"
const text_outside: String = "Venek"
# Called when the node enters the scene tree for the first time.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if player.global_position[0] < -5000:
		text = text_outside
	elif player.global_position[0] < 5000:
		text = text_ground
	else:
		text = text_first
