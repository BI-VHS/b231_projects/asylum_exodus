class_name World
extends Node2D

@export var guard_manager: GuardManager
@export var guard_path_holder: Node

@export var zone_manager: ProhibitedZoneManager
@export var zone_holder: Node

@export var patient_manager: PatientManager
@export var patient_activity_manager_holder: Node

@export var dialogue_controller: DialogueController
@export var pa_student_dialogue_resource: DialogueResource
@export var rainman_dialogue_resource: DialogueResource
@export var inspector_dialogue_resource: DialogueResource


func _ready() -> void:
	_init_zone_manager()
	_init_guard_manager()
	_init_patient_manager()
	_init_dialogues()

	guard_manager.start_day()


func _init_dialogues() -> void:
	var pa_student_container := DialogueContainer.new(
		DialogueDB.DialogueId.PA_STUDENT,
		pa_student_dialogue_resource,
		PaStudentDialogueContext.new()
	)

	var rainman_container := DialogueContainer.new(
		DialogueDB.DialogueId.RAINMAN, rainman_dialogue_resource, DialogueContext.new()
	)

	var inspector_container := DialogueContainer.new(
		DialogueDB.DialogueId.PUBLIC_TRANSPORT_INSPECTOR,
		inspector_dialogue_resource,
		PublicTransportInspectorContext.new($Inspector)
	)

	dialogue_controller.add_dialogue(pa_student_container)
	dialogue_controller.add_dialogue(rainman_container)
	dialogue_controller.add_dialogue(inspector_container)


func _init_zone_manager() -> void:
	var zones: Array[Zone]
	for zone: Zone in zone_holder.get_children():
		zones.push_back(zone)
	zone_manager.add_zones(zones)


func _init_guard_manager() -> void:
	var duties: Array[GuardDuty] = []
	for path: GuardDutyFactory in guard_path_holder.get_children():
		duties.push_back(path.create_guard_duty_from_children())
	var guards: Array[BaseGuard] = []
	for guard: BaseGuard in get_tree().get_nodes_in_group("guards"):
		guards.push_back(guard)
	guard_manager.add_duties(duties)
	guard_manager.add_guards(guards)


func _init_patient_manager() -> void:
	var patients: Array[PatientBase]
	for patient: PatientBase in get_tree().get_nodes_in_group("patients"):
		patients.push_back(patient)

	var activity_managers: Array[ActivityManager]
	for activity_manager: ActivityManager in patient_activity_manager_holder.get_children():
		activity_managers.push_back(activity_manager)

	for activity_manager: ActivityManager in activity_managers:
		patient_manager.add_activity_manager(activity_manager)

	for patient: PatientBase in patients:
		patient_manager.add_patient(patient)
