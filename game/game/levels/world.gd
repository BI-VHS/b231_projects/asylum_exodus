class_name World
extends Node2D

@export var guard_manager: GuardManager
@export var guard_path_holder: Node

@export var zone_manager: ProhibitedZoneManager
@export var zone_holder: Node

@export var patient_manager: PatientManager
@export var patient_activity_manager_holder: Node

@export var dialogue_controller: DialogueController
@export var pa_student_dialogue_resource: DialogueResource
@export var rainman_dialogue_resource: DialogueResource
@export var inspector_dialogue_resource: DialogueResource
@export var nigma_dialogue_resource: DialogueResource
@export var sampson_dialogue_resource: DialogueResource
@export var softing_dialogue_resource: DialogueResource
@export var sexy_vegetarian_dialogue_resource: DialogueResource
@export var monologue_dialogue_resource: DialogueResource


@onready var start_of_game_barrier: StaticBody2D = $StartOfGameBarrier


func _ready() -> void:
	MessageBus.start_day.connect(start)
	_init_dialogues()
	_init_zone_manager()
	# MessageBus.create_popup.emit("Další dialog: Mezerník, ESC", "Vypsat rychle dialog: ESC")
	MessageBus.dialogue_action.emit(DialogueDB.DialogueId.MONOLOGUE)
	$StartTimer.start()
	


func start() -> void:
	start_of_game_barrier.queue_free()
	#_init_zone_manager()
	_init_guard_manager()
	_init_patient_manager()
	#_init_dialogues()

	guard_manager.start_day()
	GlobalTime.start()
	
	await get_tree().create_timer(3).timeout
	MessageBus.create_popup.emit("Podívej se na mapu", "Najdeš ji v deníku")

#func _ready() -> void:
#	_init_zone_manager()
#	_init_guard_manager()
#	_init_patient_manager()
#	_init_dialogues()

#	guard_manager.start_day()


func _init_dialogues() -> void:
	var pa_student_container := DialogueContainer.new(
		DialogueDB.DialogueId.PA_STUDENT,
		pa_student_dialogue_resource,
		PaStudentDialogueContext.new()
	)
	var rainman_container := DialogueContainer.new(
		DialogueDB.DialogueId.RAINMAN, rainman_dialogue_resource, DialogueContext.new()
	)
	var inspector_container := DialogueContainer.new(
		DialogueDB.DialogueId.PUBLIC_TRANSPORT_INSPECTOR,
		inspector_dialogue_resource,
		DialogueContext.new()  # The dependency is not needed anyway
	)
	var sexy_container := DialogueContainer.new(
		DialogueDB.DialogueId.SEXY_VEGETARIAN,
		sexy_vegetarian_dialogue_resource,
		DialogueContext.new()
	)
	var sampson_container := DialogueContainer.new(
		DialogueDB.DialogueId.SAMPSON, sampson_dialogue_resource, CptSampsonDialogueContext.new()
	)
	var nigma_container := DialogueContainer.new(
		DialogueDB.DialogueId.NIGMA, nigma_dialogue_resource, EricNigmaDialogueContext.new()
	)
	var softing_container := DialogueContainer.new(
		DialogueDB.DialogueId.SOFTING, softing_dialogue_resource, DaneSoftingDialogueContext.new()
	)
	var monologue_container := DialogueContainer.new(
		DialogueDB.DialogueId.MONOLOGUE, monologue_dialogue_resource, MonologueDialogueContext.new()
	)
	

	dialogue_controller.add_dialogue(pa_student_container)
	dialogue_controller.add_dialogue(rainman_container)
	dialogue_controller.add_dialogue(inspector_container)
	dialogue_controller.add_dialogue(sexy_container)
	dialogue_controller.add_dialogue(sampson_container)
	dialogue_controller.add_dialogue(nigma_container)
	dialogue_controller.add_dialogue(softing_container)
	dialogue_controller.add_dialogue(monologue_container)


func _init_zone_manager() -> void:
	var zones: Array[Zone]
	for zone: Zone in zone_holder.get_children():
		zones.push_back(zone)
	zone_manager.add_zones(zones)


func _init_guard_manager() -> void:
	var duties: Array[GuardDuty] = []
	for path: GuardDutyFactory in guard_path_holder.get_children():
		duties.push_back(path.create_guard_duty_from_children())
	var guards: Array[BaseGuard] = []
	for guard: BaseGuard in get_tree().get_nodes_in_group("guards"):
		guards.push_back(guard)
	guard_manager.add_duties(duties)
	guard_manager.add_guards(guards)


func _init_patient_manager() -> void:
	var patients: Array[PatientBase]
	for patient: PatientBase in get_tree().get_nodes_in_group("patients"):
		patients.push_back(patient)

	var activity_managers: Array[ActivityManager]
	for activity_manager: ActivityManager in patient_activity_manager_holder.get_children():
		activity_managers.push_back(activity_manager)

	for activity_manager: ActivityManager in activity_managers:
		patient_manager.add_activity_manager(activity_manager)

	for patient: PatientBase in patients:
		print("patient: ")
		print(patient.patient_id)
		patient_manager.add_patient(patient)



func _on_start_timer_timeout():
	MessageBus.create_popup.emit("Vyzkoušej si ovládání", "Nápověda vlevo dole")
	MessageBus.start_quest.emit(QuestHandler.QuestID.QUEST1)
	await get_tree().create_timer(2).timeout
	MessageBus.create_popup.emit("Další info vlevo nahoře", "Otevři deník kliknutím (nebo Q)")
	$StartTimer.queue_free()
