extends Node2D

signal left_stairway_entered(who: CharacterBody2D)
signal right_stairway_entered(who: CharacterBody2D)

@export
var left_stairway_exit: Vector2 = $LeftStairwayExit.global_position()

@export
var right_stairway_exit: Vector2 = $LeftStairwayExit.global_position()


func enter_left_stairway(body: Node2D):
	body.position = left_stairway_exit


func enter_right_stairway(body: Node2D):
	body.position = right_stairway_exit


func _on_right_stairway_teleport_body_entered(body: Node2D):
	left_stairway_entered.emit(body)


func _on_left_stairway_teleport_body_entered(body: Node2D):
	right_stairway_entered.emit(body)
