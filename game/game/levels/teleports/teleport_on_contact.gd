class_name TeleportOnContact
extends Area2D

@export var where_node: Node2D
var where_position: Vector2:
	get:
		return where_node.global_position


func _ready() -> void:
	body_entered.connect(_on_body_entered)


func _on_body_entered(body: CharacterBody2D) -> void:
	print("teleporting", body.name)
	body.global_position = where_position
