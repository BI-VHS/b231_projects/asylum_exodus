class_name Endgame
extends Node2D

@onready var label = $CanvasLayer/Control/Reason
@onready var ui_root = $CanvasLayer/Control
@onready var animation = $CanvasLayer/Control/AnimationPlayer
@onready var sprite = $CanvasLayer/Control/AnimatedSprite2D

enum Reason{
	Death,
	Freedom,
	Catch,
}

@onready var play: bool = false

var reason_string = {
	Reason.Death: "Smrt elektřinou",
	Reason.Freedom: "Svoboda",
	Reason.Catch: "Byl jsi chycen",
}

# Called when the node enters the scene tree for the first time.
func _ready():
	ui_root.hide()
	MessageBus.endgame.connect(endgame)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if play:
		animation.play("freedom") # only looping animation
	
func endgame(reason: Reason):
	display_reason(reason)
	sprite.play("caught")
	if reason == Reason.Death:
		animation.play("death")
	elif reason == Reason.Catch:
		animation.play("caught")
	elif reason == Reason.Freedom:
		play = true
	ui_root.show()
	# get_tree().paused = true
	
func display_reason(reason: Reason):
	label.text = reason_string[reason]

func _on_button_pressed():
	get_tree().quit()


func _on_button_2_pressed():
	QuestHandler.reset_quest_handler()
	GlobalTime.reset_global_time()
	get_tree().paused = false
	get_tree().change_scene_to_file("res://game/levels/Main.tscn")
