extends Area2D

@export var level: String = "ground"

const MAP_OFFSET: int = 10_000

func teleport(body: CharacterBody2D):
	# print("teleporting", body.name)
	if level == "ground":
		body.position[0] += MAP_OFFSET
	elif level == "first":
		body.position[0] -= MAP_OFFSET


func _on_body_entered(body: CharacterBody2D):
	#body.get_slide_collision_count()
	if body is Player:
		player_variables.can_interact = true
	else:
		# npc on stairs
		# wait for 2 seconds
		await get_tree().create_timer(10).timeout
		
		# find the current npc on stairs
		for b in get_overlapping_bodies():
			if b.name == body.name:
				# teleport the specific npc that came 2 seconds ago
				teleport(body) 

func _on_body_exited(body):
	if body is Player:
		player_variables.can_interact = false
