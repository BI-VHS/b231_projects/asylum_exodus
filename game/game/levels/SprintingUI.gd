extends Control


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$ProgressBar.value = int(100 * float(player_variables.sprint_stamina) / player_variables.MAX_STAMINA)
