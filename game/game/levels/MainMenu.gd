extends Control

func _on_start_press():
	get_tree().change_scene_to_file("res://game/levels/World.tscn")
	
func _on_quit_press():
	get_tree().quit()

func _on_hover():
	$button_sound.play()


func _on_fullscreen_pressed():
	var mode = DisplayServer.window_get_mode()
	if mode == DisplayServer.WINDOW_MODE_FULLSCREEN:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_MAXIMIZED)
	elif mode == DisplayServer.WINDOW_MODE_WINDOWED or mode == DisplayServer.WINDOW_MODE_MAXIMIZED:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
