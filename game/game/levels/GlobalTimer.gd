extends Timer

# update time
func _process(_delta: float) -> void:
	$TimeInfo.text = "Čas: " + GlobalTime.time_as_string(GlobalTime.get_minutes_since_midnight())
