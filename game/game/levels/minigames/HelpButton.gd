@tool
extends Button

@export var label: Label
@onready var label_hidden = true

func _ready():
	label.hide()

func toggle():
	if label_hidden:
		label.show()
		label_hidden = false
	else:
		label.hide()
		label_hidden = true

func _on_pressed():
	toggle()
