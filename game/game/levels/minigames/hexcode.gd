extends Control

var code: Array[int]
@onready var code_box = $CodeBox
@onready var buttons1 = $VBoxContainer
@onready var buttons2 = $VBoxContainer2
@onready var buttons3 = $VBoxContainer3
@onready var buttons4 = $VBoxContainer4
var code_labels: Array[Label]
var buttons: Array


@onready var rng = RandomNumberGenerator.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	code_labels = code_box.code_labels
	buttons.push_back(buttons1.check_buttons)
	buttons.push_back(buttons2.check_buttons)
	buttons.push_back(buttons3.check_buttons)
	buttons.push_back(buttons4.check_buttons)
	
	for x in 4:
		code.push_back(rng.randi_range(0,15))
	for x in 4:
		code_labels[x].text = "%X" % [code[x]]

func check():
	for x in 4:
		var num = code[x]
		var button_array = buttons[x]
		var sum: int = 0
		for y in 4:
			var exp = 3 - y
			if button_array[y].button_pressed:
				sum += int(pow(2,exp))
		if num != sum:
			print("false")
			return
	print("true")
	MessageBus.end_minigame.emit(MinigameHandler.Minigame.Hexcode)

func _on_button_pressed():
	check()
