extends Control

@onready var disabled_breaker: int = 0


func disable_breaker(index: int):
	disabled_breaker = index
	MessageBus.end_minigame.emit(MinigameHandler.Minigame.Breakerbox)

func _on_check_button_toggled(toggled_on):
	disable_breaker(1)


func _on_check_button_2_toggled(toggled_on):
	disable_breaker(2)


func _on_check_button_3_toggled(toggled_on):
	disable_breaker(3)
