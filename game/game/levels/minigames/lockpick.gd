extends Control

var up_buttons: Array[Button]
var down_buttons: Array[Button]
var tabs: Array
var code: Array[int]
var positions: Array[int]
var current_index: int = 0
@onready var lockpick = $Lockpick
@onready var rng = RandomNumberGenerator.new()
@onready var label = $Label


# Called when the node enters the scene tree for the first time.
func _ready():
	up_buttons.push_back($Up)
	up_buttons.push_back($Up2)
	up_buttons.push_back($Up3)
	up_buttons.push_back($Up4)
	up_buttons.push_back($Up5)
	
	down_buttons.push_back($Down)
	down_buttons.push_back($Down2)
	down_buttons.push_back($Down3)
	down_buttons.push_back($Down4)
	down_buttons.push_back($Down5)
	
	tabs.push_back($Tab)
	tabs.push_back($Tab2)
	tabs.push_back($Tab3)
	tabs.push_back($Tab4)
	tabs.push_back($Tab5)
	
	for c in 5:
		code.push_back(rng.randi_range(0,4))
		
	print(code)
	
	for c in code:
		label.text += str(c)
		
	for x in 5:
		positions.push_back(0)
		
	hide_other_buttons(0)

func up_press(index: int):
	tabs[index].position.y -= 52
	tabs[index].position.y = clamp(tabs[index].position.y, 400, 660)
	positions[index] = int(5 - (tabs[index].position.y - 400)/52)
	print(tabs[index].position.y, " ", positions[index])
	$Label2.text = str(positions[index])

func down_press(index: int):
	tabs[index].position.y += 52
	tabs[index].position.y = clamp(tabs[index].position.y, 400, 660)
	positions[index] = int(5 - (tabs[index].position.y - 400)/52)
	print(tabs[index].position.y, " ", positions[index])
	$Label2.text = str(positions[index])
	
func hide_other_buttons(index: int):
	for x in 5:
		up_buttons[x].hide()
		down_buttons[x].hide()
	
	up_buttons[index].show()
	down_buttons[index].show()
	current_index = index
	
func reset_position(index):
	tabs[index].position.y = 660
	positions[index] = 0
	
func check_position():
	if positions[current_index] == code[current_index]:
		if current_index == 4:
			print("Done")
			MessageBus.end_minigame.emit(MinigameHandler.Minigame.Lockpick)
			return
		current_index += 1
		hide_other_buttons(current_index)
		$Label2.text = str(positions[current_index])
		lockpick.position.x = -464 + current_index*130
	else:
		for x in 5:
			reset_position(x)
		hide_other_buttons(0)
		$Label2.text = str(positions[current_index])
		lockpick.position.x = -464

func _on_up_pressed():
	up_press(0)

func _on_up_2_pressed():
	up_press(1)

func _on_up_3_pressed():
	up_press(2)

func _on_up_4_pressed():
	up_press(3)

func _on_up_5_pressed():
	up_press(4)

func _on_down_pressed():
	down_press(0)

func _on_down_2_pressed():
	down_press(1)

func _on_down_3_pressed():
	down_press(2)

func _on_down_4_pressed():
	down_press(3)

func _on_down_5_pressed():
	down_press(4)

func _on_check_pressed():
	check_position()
