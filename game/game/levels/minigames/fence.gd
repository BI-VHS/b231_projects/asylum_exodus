extends Control

signal cut
var boxes: Array
@onready var fence_active: bool = true

# Called when the node enters the scene tree for the first time.
func _ready():
	cut.connect(check)
	boxes = $Boxes.get_children()
	$Button.hide()


func hide_all():
	for b in boxes:
		b.hide()
	$Fence.texture = preload("res://sprites/fence/fence_big_hole.png")
	$Button.show()

func check():
	if fence_active:
		MessageBus.end_minigame.emit(MinigameHandler.Minigame.Fence)
		
	for b in boxes:
		if not b.button_pressed:
			print("false")
			return
	print("true")
	hide_all()


func _on_button_pressed():
	MessageBus.end_minigame.emit(MinigameHandler.Minigame.Fence)
