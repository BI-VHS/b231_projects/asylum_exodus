extends Control

@onready var sliders: Array[TextureRect]
@onready var in_use: Array[bool]
@onready var animplayers: Array[AnimationPlayer]
@onready var queue: Array[String]
@onready var timers: Array[Timer]

signal slid_in()

# Called when the node enters the scene tree for the first time.
func _ready():
	sliders.push_back($Slider)
	sliders.push_back($Slider2)
	sliders.push_back($Slider3)
	
	animplayers.push_back($AnimationPlayer)
	animplayers.push_back($AnimationPlayer2)
	animplayers.push_back($AnimationPlayer3)
	
	timers.push_back($Timer)
	timers.push_back($Timer2)
	timers.push_back($Timer3)
	
	for s in sliders:
		in_use.push_back(false)
		
	slid_in.connect(check_in_use)
	MessageBus.create_popup.connect(queue_message)

func push(title: String, body: String):
	queue.push_back(title)
	queue.push_back(body)
	
func pop() -> Array[String]:
	var title = queue.pop_front()
	var body = queue.pop_front()
	return [title, body]
	
func slide_out(index: int, title: String, body: String):
	in_use[index] = true
	sliders[index].title.text = title
	sliders[index].body.text = body
	animplayers[index].play("out")
	
func slide_in(index: int):
	animplayers[index].play("in")

func queue_message(title: String, body: String):
	print("queueing message")
	push(title, body)
	check_in_use()

func check_in_use():
	if not queue.is_empty():
		var index: int = -1
		
		for i in 3:
			if not in_use[i]:
				index = i
				break
		if index == -1:
			return
		print("printing popup")
		var text: Array[String] = pop()
		var title: String = text[0]
		var body: String = text[1]
		slide_out(index, title, body)

func _on_animation_player_animation_finished(anim_name):
	if anim_name == "in":
		in_use[0] = false
		slid_in.emit()
	elif anim_name == "out":
		timers[0].start()

func _on_animation_player_2_animation_finished(anim_name):
	if anim_name == "in":
		in_use[1] = false
		slid_in.emit()
	elif anim_name == "out":
		timers[1].start()

func _on_animation_player_3_animation_finished(anim_name):
	if anim_name == "in":
		in_use[2] = false
		slid_in.emit()
	elif anim_name == "out":
		timers[2].start()

func _on_timer_timeout():
	slide_in(0)

func _on_timer_2_timeout():
	slide_in(1)

func _on_timer_3_timeout():
	slide_in(2)
