extends StaticBody2D
## Intented Usecase of script:
## This script assumes the following:
## it is placed on a StaticBody2D node, that is the top node in the .tscn file
## this node has a metadata of int type called "StartFrame"
## this node has a child with the default "AnimatedSprite2D" name of AnimatedSprite2D type
## this child has the animation under the default name "default"

@onready var sprite := get_node("AnimatedSprite2D") as AnimatedSprite2D
## Called when the node enters the scene tree for the first time.
func _ready():
	sprite.frame = self.get_meta("StartFrame") % sprite.sprite_frames.get_frame_count("default")

## Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	sprite.play("default")
