@tool
class_name SleepActionable
extends Actionable

@export var player: Player
@onready var debug: bool = global_variables.debug
@onready var default_global_minute: float = GlobalTime.wait_time
@export var sleep_ui: Control
@export var sleep_global_minute: float = 0.01
@export var patient_manager: PatientManager
var is_sleeping: bool = false
var popup_block: bool = false
var new_day: bool = false
var current_time: Vector2


func _ready():
	GlobalTime.update.connect(update_time)
	GlobalTime.day_end.connect(set_new_day)

func update_time(new_time: Vector2):
	current_time = new_time
	if is_sleeping and current_time[0] >= 7 and new_day:
		wake_up()

func send_no_sleep_popup():
	if popup_block:
		return
	popup_block = true
	$PopupTimeout.start()
	MessageBus.create_popup.emit("Teď nemůžeš spát", "Spát můžeš až od půlnoci")

func set_new_day():
	new_day = true
	
func reset_npcs():
	var patients: Array = get_tree().get_nodes_in_group("patients")
	for pat: PatientBase in patients:
		patient_manager.remove_patient(pat.patient_id)
	for pat: PatientBase in patients:
		patient_manager.add_patient(pat)

func wake_up():
	new_day = false
	is_sleeping = false
	player.paused = false
	sleep_ui.hide()
	GlobalTime.one_minute_duration = default_global_minute
	reset_npcs()

func action() -> void:
	if not debug:
		if not current_time[0] < 7:
			send_no_sleep_popup()
			return
	print("going to sleep, goodnight")
	sleep_ui.show()
	player.paused = true
	GlobalTime.one_minute_duration = sleep_global_minute
	is_sleeping = true


func _on_popup_timeout_timeout():
	popup_block = false
