class_name Actionable
extends Area2D

## Base class for Actionable items, does not implement any behaviour.
## A player has ActionableFinder, which highlights neares actionable and runs
## its action() upon pressing the action keybind.

@export var interaction_name: String = ""


func action() -> void:
	pass
