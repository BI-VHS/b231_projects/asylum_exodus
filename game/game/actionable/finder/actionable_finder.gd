class_name ActionableFinder
extends Area2D

## Keeps track of Actionables around Player and tells InteractionUI when to show
## possible interactions based on being enabled and if there is a closest Actionable present.

const InteractUIClass = preload("res://ui/actionable/interact_ui.tscn")

var actionables: Array[Actionable]:
	get:
		var result: Array[Actionable] = []
		for actionable in get_overlapping_areas():
			result.push_back(actionable)
		return result

var closest_actionable: Actionable:
	get:
		if actionables.size() <= 0:
			return null

		var pick_closest = func(a: Area2D, b: Area2D): return (
			a
			if (
				global_position.distance_to(a.global_position)
				< global_position.distance_to(b.global_position)
			)
			else b
		)
		return actionables.reduce(pick_closest)

var _interact_ui_shown_on: Actionable = null

var _interact_ui: InteractUI

var _enabled: bool = true


func disable() -> void:
	_enabled = false
	_hide_interact_ui()


func enable() -> void:
	_enabled = true


func interact_with_closest_actionable() -> void:
	if closest_actionable != null:
		closest_actionable.action()


func _ready() -> void:
	_interact_ui = InteractUIClass.instantiate()
	add_child(_interact_ui)


func _physics_process(_delta: float) -> void:
	if not _enabled:
		return

	if actionables.is_empty():
		_hide_interact_ui()
	else:
		_show_interact_ui()


func _show_interact_ui() -> void:
	var closest = closest_actionable
	if closest == _interact_ui_shown_on:
		return

	_interact_ui_shown_on = closest
	_interact_ui.show_interaction(closest)


func _hide_interact_ui() -> void:
	_interact_ui.hide_interaction()
	_interact_ui_shown_on = null
