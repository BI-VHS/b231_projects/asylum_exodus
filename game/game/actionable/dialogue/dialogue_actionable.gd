class_name DialogueActionable
extends Actionable

## Actionable, which requests a dialogue to be played defined by it's DialogueId

@export var dialogue_id: DialogueDB.DialogueId


func action() -> void:
	MessageBus.dialogue_action.emit(dialogue_id)
