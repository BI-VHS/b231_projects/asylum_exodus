class_name PickupActionable
extends Actionable

var item: StaticBody2D
var quest: QuestHandler.QuestID
var condition: String
var sound: AudioStreamPlayer2D

var expose_index: int
var mark_previous: int
var create_pop: bool
var tit: String
var bod: String

var collider: CollisionShape2D

## constructor
func constructor(scene_path: String, position: Vector2, assigned_quest: QuestHandler.QuestID, 
				 cond: String, interact_name: String, parent: Node, expose_condition_index: int = -1,
				 mark_prev: int = -1, create_label: bool = false, create_popup: bool = false, 
				 title: String = "", body: String = ""):
	var scene = load(scene_path)
	item = scene.instantiate()
	add_child(item)
	global_position = position
	quest = assigned_quest
	condition = cond
	interaction_name = interact_name
	
	sound = AudioStreamPlayer2D.new()
	sound.stream = preload("res://sounds/item-pickup.mp3")
	add_child(sound)
	expose_index = expose_condition_index
	mark_previous = mark_prev
	create_pop = create_popup
	tit = title
	bod = body
	
	var shape = CircleShape2D.new()
	shape.radius = 45
	collider = CollisionShape2D.new()
	collider.shape = shape
	collision_layer = 16
	add_child(collider)
	
	if create_label:
		var label = Label.new()
		label.text = "!"
		label.horizontal_alignment = HORIZONTAL_ALIGNMENT_CENTER
		label.vertical_alignment = VERTICAL_ALIGNMENT_TOP
		
		var settings: LabelSettings = LabelSettings.new()
		settings.font_size = 100
		settings.font = preload("res://ui/fonts/PixelifySans-VariableFont_wght.ttf")
		settings.font_color = Color(0,1,0,1)
		settings.shadow_color = Color(0,0,0,1)
		
		label.label_settings = settings

		label.scale = Vector2(0.7,0.7)
		label.position.y -= 50
		add_child(label)
	
	parent.add_child(self)
	
	
## item pickup
func action():
	sound.play()
	if expose_index != -1:
		MessageBus.expose_condition.emit(expose_index)
	if mark_previous != -1:
		MessageBus.mark_checked.emit(mark_previous)
	if create_pop:
		MessageBus.create_popup.emit(tit, bod)
	MessageBus.condition_done.emit(quest, condition)
	item.hide()
	collider.queue_free()
	await sound.finished
	queue_free()
