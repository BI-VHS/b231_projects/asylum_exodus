class_name MinigameActionable
extends Actionable

var minigame_trigger: MinigameHandler.Minigame

func constructor(pos: Vector2, parent: Node, interact_name: String, minigame: MinigameHandler.Minigame, create_label: bool = false):
	global_position = pos
	interaction_name = interact_name
	minigame_trigger = minigame
	
	var shape = CircleShape2D.new()
	shape.radius = 45
	var collider = CollisionShape2D.new()
	collider.shape = shape
	collision_layer = 16
	add_child(collider)
	
	if create_label:
		var label = Label.new()
		label.text = "!"
		label.horizontal_alignment = HORIZONTAL_ALIGNMENT_CENTER
		label.vertical_alignment = VERTICAL_ALIGNMENT_TOP
		
		var settings: LabelSettings = LabelSettings.new()
		settings.font_size = 100
		settings.font = preload("res://ui/fonts/PixelifySans-VariableFont_wght.ttf")
		settings.font_color = Color(0,1,0,1)
		settings.shadow_color = Color(0,0,0,1)
		
		label.label_settings = settings

		label.scale = Vector2(0.7,0.7)
		label.position.y -= 50
		add_child(label)
	
	parent.add_child(self)

func action():
	MessageBus.start_minigame.emit(minigame_trigger)
	queue_free()
