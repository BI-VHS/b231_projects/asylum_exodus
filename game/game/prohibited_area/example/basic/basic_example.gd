extends Node2D

@export var zone_config_gf_west_prohibited: ProhibitedZoneConfig
@export var zone_config_gf_west_non_prohibited: ProhibitedZoneConfig
var zones: Array[Zone]

var _every_30_frames: int = 0

@onready var player = %Player

@onready var zone_manager: ProhibitedZoneManager = %ProhibitedZoneManager

@onready var guard_manager: GuardManager = $GuardManager


func _ready() -> void:
	for zone in get_tree().get_nodes_in_group("zone"):
		zones.push_back(zone)

	zone_manager.add_zones(zones)
	zone_manager.apply_configuration(zone_config_gf_west_prohibited)


func _get_duties(factory_holder: Node) -> Array[GuardDuty]:
	var factories: Array[GuardDutyFactory] = []
	for factory in factory_holder.get_children():
		factories.push_back(factory)

	var duties: Array[GuardDuty] = []
	for duty in factories.map(func(factory): return factory.create_guard_duty_from_children()):
		duties.push_back(duty)

	return duties


func _physics_process(_delta: float) -> void:
	_every_30_frames += 1
	if not _every_30_frames >= 30:
		return

	_every_30_frames = 0
	if zone_manager.get_zone_id_by_position(player.position) != -1:
		print("Player dected in zone by position")

	if zone_manager.get_zone_containing_body(player) != null:
		print("Player dected in zone by body")
