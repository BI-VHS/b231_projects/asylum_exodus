extends Node2D

## TODO: BaseGuard keeps a list, which is hard to controll when changing prohibited areas

@export var gf_west_hall_prohibited: ProhibitedZoneConfig
@export var gf_west_hall_non_prohibited: ProhibitedZoneConfig

var _every_30_frames: int = 0

@onready var guard_manager: GuardManager = $GuardManager
@onready var zone_manager: ProhibitedZoneManager = $ProhibitedZoneManager

@onready var player: Player = $Player
@onready var time_info: Label = %TimeInfo


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("interact"):
		if zone_manager.is_zone_prohibited(Zone.ZoneId.GF_WEST_HALL):
			zone_manager.apply_configuration(gf_west_hall_non_prohibited)
		else:
			zone_manager.apply_configuration(gf_west_hall_prohibited)


func _ready() -> void:
	GlobalTime.one_minute_duration = 1 / 4
	var guards: Array[BaseGuard]
	for guard in get_tree().get_nodes_in_group("guards"):
		guards.push_back(guard)

	var duties: Array[GuardDuty] = _get_duties_by_factory_holder($FactoryHolder)

	guard_manager.add_guards(guards)
	guard_manager.add_duties(duties)
	guard_manager.start_day()

	var zones: Array[Zone]
	for zone in get_tree().get_nodes_in_group("zone"):
		zones.push_back(zone)

	zone_manager.add_zones(zones)
	zone_manager.apply_configuration(gf_west_hall_prohibited)


func _physics_process(_delta: float) -> void:
	time_info.text = "Clock: " + GlobalTime.time_as_string(GlobalTime.get_minutes_since_midnight())


func _get_duties_by_factory_holder(factory_holder: Node) -> Array[GuardDuty]:
	var factories: Array[GuardDutyFactory] = []
	for factory in factory_holder.get_children():
		factories.push_back(factory)

	var duties: Array[GuardDuty] = []
	for duty in factories.map(func(factory): return factory.create_guard_duty_from_children()):
		duties.push_back(duty)

	return duties
