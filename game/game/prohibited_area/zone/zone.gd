class_name Zone
extends Area2D

## Please use as children only CollisionShape2Ds with RectancularShape2D

signal body_entered_plus(body: Node, zone: Zone)
signal body_exited_plus(body: Node, zone: Zone)
signal area_entered_plus(area: Area2D, zone: Zone)
signal area_exited_plus(area: Area2D, zone: Zone)

enum ZoneId {
	# Ground floor:
	GF_HALL,
	GF_ARCHIVE,
	GF_ELEVATOR,
	GF_JANITOR_ROOM,
	GF_RECEPTION,
	GF_DIRECTORS_OFFICE,
	GF_EMPLOYEE_OFFICE,
	GF_CLEANING_ROOM,
	GF_STORE_ROOM,
	GF_KITCHEN,
	GF_CANTEEN,
	GF_WC,
	GF_SECURITY_SLEEPING_ROOM,
	GF_WORKSHOP,
	# First floor:
	FF_HALL,
	FF_CINEMA,
	FF_ELEVATOR,
	FF_SECURITY_ROOM,
	FF_SLEEPING_ROOM,
	FF_WC,
	FF_LIBRARY,
	FF_JANITOR_ROOM,
	FF_INFIRMARY,
	FF_PSYCHIATRIST,
	FF_OPERATION_ROOM,
	FF_DOCTORS_ROOM,
	FF_MEDICATION_STORAGE,
	FF_COMMON_ROOM,
	O_OUTSIDE,
}

const PROHIBITED := true
const NON_PROHIBITED := false

@export var zone_id: ZoneId

var is_prohibited: bool


func _init() -> void:
	body_entered.connect(func(body: Node): body_entered_plus.emit(body, self))
	body_exited.connect(func(body: Node): body_exited_plus.emit(body, self))
	area_entered.connect(func(area: Area2D): area_entered_plus.emit(area, self))
	area_exited.connect(func(area: Area2D): area_exited_plus.emit(area, self))


func get_collision_shapes() -> Array[CollisionShape2D]:
	var shapes: Array[CollisionShape2D] = []
	for shape in get_children().filter(func(it: Node): return it is CollisionShape2D):
		shapes.push_back(shape)
	return shapes
	
var sneak2: bool = false
var sneak3: bool = false
var sneak4: bool = false

func _on_body_entered_plus(body: Node, zone: Zone):
	if QuestHandler.current_quest == 1 and not QuestHandler.quest1["poison"]:
		if body.name == "PlayerCamera":
			if zone.zone_id == ZoneId.GF_CANTEEN:
				MessageBus.create_popup.emit("Jídlo otrávíš interakcí", "Přibliž se a zmáčkni E")
	
	if QuestHandler.current_quest == 2 and not sneak2:
		if body.name == "PlayerCamera":
			if zone.zone_id == ZoneId.GF_JANITOR_ROOM:
				sneak2 = true
				MessageBus.condition_done.emit(QuestHandler.QuestID.QUEST2, "lure_out")
				MessageBus.condition_done.emit(QuestHandler.QuestID.QUEST2, "sneak_in")
				MessageBus.mark_checked.emit(1)
				MessageBus.mark_checked.emit(2)
				MessageBus.expose_condition.emit(3)
				MessageBus.create_popup.emit("Podmínka splněna", "Propliž se do uklízečovi místnosti")
				
	if QuestHandler.current_quest == 3 and not sneak3:
		if body.name == "PlayerCamera":
			if zone.zone_id == ZoneId.FF_SECURITY_ROOM:
				sneak3 = true
				MessageBus.condition_done.emit(QuestHandler.QuestID.QUEST3, "find_remote")
				MessageBus.mark_checked.emit(1)
				MessageBus.expose_condition.emit(2)
				MessageBus.create_popup.emit("Podmínka splněna", "Najdi ovladač")
				
	if QuestHandler.current_quest == 4 and not sneak4:
		if body.name == "PlayerCamera":
			if zone.zone_id == ZoneId.FF_MEDICATION_STORAGE:
				sneak4 = true
				MessageBus.condition_done.emit(QuestHandler.QuestID.QUEST4, "sneak_in")
				MessageBus.mark_checked.emit(2)
				MessageBus.expose_condition.emit(3)
				MessageBus.create_popup.emit("Podmínka splněna", "Propliž se do skladu s léky")
				
	#if QuestHandler.current_quest == 2:
	#	if body.name == "Janitor":
	#		if zone.zone_id == ZoneId.GF_HALL:
	#			MessageBus.condition_done.emit(QuestHandler.QuestID.QUEST4, "return")
