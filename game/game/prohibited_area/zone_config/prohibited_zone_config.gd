class_name ProhibitedZoneConfig
extends Resource

const PROHIBITED: bool = Zone.PROHIBITED
const NON_PROHIBITED: bool = Zone.NON_PROHIBITED

## ZoneId -> bool, defines if a Zone with given Id is prohibited
## Always contains the full definition -> all Ids have a bool defined for them
var config: Dictionary:
	get:
		return _get_config()


func _get_config() -> Dictionary:
	assert(false, "ProhibitedZoneConfig is a base class and its method should not be used!")
	return {}


## Must be given a Dictionary of ZoneId -> Zone
func apply_config_on_zones_dict(zone_id_to_zone: Dictionary) -> void:
	for id in config.keys():
		var is_prohibited = config[id]
		var zone: Zone = zone_id_to_zone.get(id)
		if zone == null:
			printerr(
				"Zone with id: " + str(id) + " was not defined, but configuration tried to set it"
			)
		else:
			zone.is_prohibited = is_prohibited
