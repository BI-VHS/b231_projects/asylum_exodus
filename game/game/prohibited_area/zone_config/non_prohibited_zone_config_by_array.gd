class_name NonProhibitedZoneConfigByArray
extends ProhibitedZoneConfig

## All zones in 'non_prohibited_zone_ids' array will be non-prohibited, rest prohibited

@export var non_prohibited_zone_ids: Array[Zone.ZoneId]

var _is_backing_field_set: bool = false
var _config_backing_field: Dictionary = {}


func _get_config() -> Dictionary:
	if _is_backing_field_set:
		return _config_backing_field

	for zone_id in Zone.ZoneId.values():
		_config_backing_field[zone_id] = PROHIBITED

	for non_prohibited_zone_id in non_prohibited_zone_ids:
		_config_backing_field[non_prohibited_zone_id] = NON_PROHIBITED

	_is_backing_field_set = true

	return _config_backing_field
