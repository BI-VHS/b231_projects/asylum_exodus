class_name ProhibitedZoneDailyConfig
extends Resource

## In future refactor together with all schedules

@export_category("Times")
@export_subgroup("Sleep before midnight")
@export_range(0, 23) var sleep_before_midnight_from_hours: int = 23
@export_range(0, 60) var sleep_before_midnight_from_minutes: int = 30
@export_range(0, 23) var sleep_before_midnight_to_hours: int = 23
@export_range(0, 60) var sleep_before_midnight_to_minutes: int = 59

@export_subgroup("Sleep after midnight")
@export_range(0, 23) var sleep_after_midnight_from_hours: int = 0
@export_range(0, 60) var sleep_after_midnight_from_minutes: int = 0
@export_range(0, 23) var sleep_after_midnight_to_hours: int = 7
@export_range(0, 60) var sleep_after_midnight_to_minutes: int = 0

@export_subgroup("Pills")
@export_range(0, 23) var pills_from_hours: int = 7
@export_range(0, 60) var pills_from_minutes: int = 0
@export_range(0, 23) var pills_to_hours: int = 12
@export_range(0, 60) var pills_to_minutes: int = 30

@export_subgroup("Food")
@export_range(0, 23) var food_from_hours: int = 12
@export_range(0, 60) var food_from_minutes: int = 30
@export_range(0, 23) var food_to_hours: int = 14
@export_range(0, 60) var food_to_minutes: int = 0

@export_subgroup("Free time")
@export_range(0, 23) var free_time_from_hours: int = 14
@export_range(0, 60) var free_time_from_minutes: int = 0
@export_range(0, 23) var free_time_to_hours: int = 19
@export_range(0, 60) var free_time_to_minutes: int = 00

@export_subgroup("Dinner")
@export_range(0, 23) var dinner_from_hours: int = 19
@export_range(0, 60) var dinner_from_minutes: int = 00
@export_range(0, 23) var dinner_to_hours: int = 21
@export_range(0, 60) var dinner_to_minutes: int = 30

@export_subgroup("Brush teeth")
@export_range(0, 23) var brush_teeth_from_hours: int = 21
@export_range(0, 60) var brush_teeth_from_minutes: int = 30
@export_range(0, 23) var brush_teeth_to_hours: int = 23
@export_range(0, 60) var brush_teeth_to_minutes: int = 30

@export_category("Configs")
@export var sleeping_config: ProhibitedZoneConfig
@export var pills_config: ProhibitedZoneConfig
@export var food_config: ProhibitedZoneConfig
@export var free_time_config: ProhibitedZoneConfig
@export var brush_teeth_config: ProhibitedZoneConfig

## Hard coded values, because we are in a hurry :DD
var sleeping_time_before_midnight: IntRange = IntRange.new(
	_to_minutes(sleep_before_midnight_from_hours, sleep_before_midnight_from_minutes),
	_to_minutes(sleep_after_midnight_to_hours, sleep_before_midnight_to_minutes)
)
var sleeping_time_after_midnight: IntRange = IntRange.new(
	_to_minutes(sleep_after_midnight_from_hours, sleep_after_midnight_from_minutes),
	_to_minutes(sleep_after_midnight_to_hours, sleep_after_midnight_to_minutes)
)
var pills_time: IntRange = IntRange.new(
	_to_minutes(pills_from_hours, pills_from_minutes), _to_minutes(pills_to_hours, pills_to_minutes)
)
var food_time: IntRange = IntRange.new(
	_to_minutes(food_from_hours, food_from_minutes), _to_minutes(food_to_hours, food_to_minutes)
)
var free_time: IntRange = IntRange.new(
	_to_minutes(free_time_from_hours, free_time_from_minutes),
	_to_minutes(free_time_to_hours, free_time_to_minutes)
)
var brush_teeth_time: IntRange = IntRange.new(
	_to_minutes(brush_teeth_from_hours, brush_teeth_from_minutes),
	_to_minutes(brush_teeth_to_hours, brush_teeth_to_minutes)
)
var dinner_time: IntRange = IntRange.new(
	_to_minutes(dinner_from_hours, dinner_from_minutes),
	_to_minutes(dinner_to_hours, dinner_to_minutes)
)


func get_config(time: int) -> ProhibitedZoneConfig:
	if (
		sleeping_time_after_midnight.contains_left_inclusive(time)
		or sleeping_time_before_midnight.contains_inclusive(time)
	):  # inclusive to not fail at 1339
		return sleeping_config
	if pills_time.contains_left_inclusive(time):
		return pills_config
	if food_time.contains_left_inclusive(time):
		return food_config
	if free_time.contains_left_inclusive(time):
		return free_time_config
	if brush_teeth_time.contains_left_inclusive(time):
		return brush_teeth_config
	if dinner_time.contains_left_inclusive(time):
		return food_config

	printerr("No prohibited zone config was found at time: ", time)
	return null


func _to_minutes(hours: int, minutes: int) -> int:
	return hours * GlobalTime.MINUTES_IN_HOUR + minutes
