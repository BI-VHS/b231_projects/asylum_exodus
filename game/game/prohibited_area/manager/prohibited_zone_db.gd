class_name ProhibitedZoneDB
extends RefCounted

## Keeps ZoneId -> Zone and Body -> Zone sets

const PROHIBITED: bool = Zone.PROHIBITED
const NON_PROHIBITED: bool = Zone.NON_PROHIBITED

## ZoneId -> Zone
var _zones: Dictionary = {}

## Maps bodies to their zones
var _body_to_zone: Dictionary = {}

## ZoneId -> CollisionShape2D
var _zone_to_collision_shape: Dictionary = {}


func apply_configuration(zone_configuration: ProhibitedZoneConfig) -> void:
	zone_configuration.apply_config_on_zones_dict(_zones)


func get_zone_by_id(zone_id: Zone.ZoneId) -> Zone:
	return _zones[zone_id]


## Returns null on not finding anything
func get_zone_by_body(body: PhysicsBody2D) -> Zone:
	return _get_zone_by_body_slow(body)


func get_all_zones() -> Array[Zone]:
	var zones: Array[Zone] = []
	for zone in _zones.values():
		zones.push_back(zone)

	return zones


func add_zones(zones: Array[Zone]) -> void:
	for zone: Zone in zones:
		_zones[zone.zone_id] = zone
		_zone_to_collision_shape[zone.zone_id] = zone.get_collision_shapes()
		## Init Body -> Zone
		for body in zone.get_overlapping_bodies():
			if not body is StaticBody2D:
				_body_to_zone[body] = zone
		zone.body_entered_plus.connect(_on_zone_body_entered)
		zone.body_exited_plus.connect(_on_zone_body_exited)


func get_zones_to_collision_shapes() -> Dictionary:
	return _zone_to_collision_shape.duplicate()


func _on_zone_body_entered(body: Node, zone: Zone) -> void:
	_body_to_zone[body] = zone


func _on_zone_body_exited(body: Node, _zone: Zone) -> void:
	_body_to_zone.erase(body)
	#if QuestHandler.current_quest == 2:
	#	if QuestHandler.quest2["lure_out"] == false:
	#		if body.name == "Janitor" and _zone.zone_id == Zone.ZoneId.GF_JANITOR_ROOM:
	#			MessageBus.condition_done.emit(QuestHandler.QuestID.QUEST2, "lure_out")
	#			MessageBus.mark_checked.emit(1)
	#			MessageBus.create_popup.emit("Podmínka splněna", "Zaměstnej uklízeče")

func _get_zone_by_body_fast(body: PhysicsBody2D) -> Zone:
	return _body_to_zone.get(body)


func _get_zone_by_body_slow(body: PhysicsBody2D) -> Zone:
	var body_to_zone_result: Zone = _get_zone_by_body_fast(body)
	if body_to_zone_result != null:
		return body_to_zone_result

	for zone: Zone in _zones.values():
		if zone.overlaps_body(body):
			return zone
	return null
