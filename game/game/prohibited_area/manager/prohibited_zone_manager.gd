class_name ProhibitedZoneManager
extends Node2D

## Prohibited zones are Area2Ds, with CollsisionShape2D defining the zone and
## they need to have RectangleShape2D inside of them. Then we just listen to
## collisions on Layer/Mask "Zone".

const PROHIBITED: bool = Zone.PROHIBITED
const NON_PROHIBITED: bool = Zone.NON_PROHIBITED

@export var daily_config: ProhibitedZoneDailyConfig
var active_config: ProhibitedZoneConfig

var _zone_db: ProhibitedZoneDB = ProhibitedZoneDB.new()


func _process(_delta: float) -> void:
	if daily_config == null || Engine.get_process_frames() % 15 != 0:
		return
	var time: int = GlobalTime.get_minutes_since_midnight()
	var new_config: ProhibitedZoneConfig = daily_config.get_config(time)
	if not new_config == null and (active_config == null or active_config != new_config):
		active_config = new_config
		apply_configuration(active_config)


func add_zones(zones: Array[Zone]) -> void:
	_zone_db.add_zones(zones)


func prohibit_zone(zone_id: Zone.ZoneId) -> void:
	_zone_db.get_zone(zone_id).is_prohibited = PROHIBITED


func allow_zone(zone_id: Zone.ZoneId) -> void:
	_zone_db.get_zone(zone_id).is_prohibited = NON_PROHIBITED


func apply_configuration(zone_configuration: ProhibitedZoneConfig) -> void:
	_zone_db.apply_configuration(zone_configuration)


func get_zone_by_id(zone_id: Zone.ZoneId) -> Zone:
	return _zone_db.get_zone_by_id(zone_id)


func get_zone_containing_body(body: PhysicsBody2D) -> Zone:
	return _zone_db.get_zone_by_body(body)


func is_point_in_prohibited_zone(point: Vector2) -> bool:
	var zone_id: Zone.ZoneId = get_zone_id_by_position(point)
	if zone_id == -1:
		return false
	return get_zone_by_id(zone_id).is_prohibited


func is_body_in_prohibited_zone(body: Node) -> bool:
	var zone: Zone = get_zone_containing_body(body)
	if zone == null:
		return false

	return zone.is_prohibited


## Returns true if zone with given ID is currently prohibited.
func is_zone_prohibited(zone_id: Zone.ZoneId) -> bool:
	return _zone_db.get_zone_by_id(zone_id).is_prohibited


## Returns true if zone with given ID is not currently prohibited.
func is_zone_not_prohibited(zone_id: Zone.ZoneId) -> bool:
	return not is_zone_prohibited(zone_id)


## Given a position in the world returns a zone containing that location.
## Please use global position.
## Returns -1 if the position is not contained in any defined zone.
func get_zone_id_by_position(point: Vector2) -> Zone.ZoneId:
	var zone_to_collision_shape: Dictionary = _zone_db.get_zones_to_collision_shapes()

	for zone_id in zone_to_collision_shape.keys():
		var collision_shapes: Array[CollisionShape2D] = zone_to_collision_shape[zone_id]
		if collision_shapes.any(
			func(shape: CollisionShape2D): return shape.shape.get_rect().has_point(point)
		):
			printerr("Found zone: ", EnumPrintHelpers.name_by_id(zone_id, Zone.ZoneId))
			return zone_id

	return -1


## Return null on point not being in any Zone
func get_zone_by_position(point: Vector2) -> Zone:
	var zone_id: Zone.ZoneId = get_zone_id_by_position(point)
	if zone_id == -1:
		return null
	return get_zone_by_id(zone_id)
