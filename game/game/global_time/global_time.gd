extends Timer

signal day_end
signal update(time: Vector2)

const MINUTES_IN_HOUR: int = 60
const MINUTES_IN_DAY: int = 1440  # 60 * 24

var global_minute: float = self.one_minute_duration

@export var _minutes_total: int = 660

func reset_global_time():
	stop()
	_minutes_total = 660

## How many seconds in real life does a minute last in-game
## Setting does not work god knows why
var one_minute_duration: float:
	get:
		return wait_time
	set(value):
		print("setting global minute", value)
		global_minute = value


## Use minutes since midnight as argument
func time_as_string(minutes: int) -> String:
	var hours = minutes / 60

	# Custom function to pad the string with zeros
	var formatted_hours = str(hours).pad_zeros(2)
	var formatted_minutes = str(minutes % MINUTES_IN_HOUR).pad_zeros(2)

	return formatted_hours + ":" + formatted_minutes

## Use minutes since midnight as argument
## Returns [0] hours, [1] minutes
func time_as_vector2(minutes: int) -> Vector2:
	return Vector2(minutes / 60, minutes % MINUTES_IN_HOUR)


## Gets minutes elapsed since 00:00
func get_minutes_since_midnight() -> int:
	return _minutes_total % MINUTES_IN_DAY


## Gets the amount of days since the first day
func get_day() -> int:
	return _minutes_total / MINUTES_IN_DAY


func how_many_real_seconds_between(from: int, to: int) -> float:
	if to < from:
		return -1
	return (to - from) * one_minute_duration


## Returns how long in real time seconds would it take to arrive at given in-game time
## assuming that we were to wait starting at current in-game time.
## If until time is lower than current time or higher than possible time in day, return -1.
func how_long_since_now_in_real_time_in_single_day(until: int) -> float:
	if until >= MINUTES_IN_DAY:
		return -1
	return how_many_real_seconds_between(get_minutes_since_midnight(), until)


## Sets current time to different value. Will most likely break something if done carelessly!
func set_time(minutes: int) -> void:
	assert(minutes >= 0 && minutes <= MINUTES_IN_HOUR)
	_minutes_total = minutes


## Please don't use minutes value over 59, instead add 1 hour. Use with caution.
func set_time_with_hours(minutes: int, hours: int) -> void:
	assert(minutes >= 0 && minutes <= 59 && hours >= 0 && hours <= 23)
	_minutes_total = minutes + hours * MINUTES_IN_HOUR


func _ready() -> void:
	# one_shot = false
	timeout.connect(_minute_passed)


func _minute_passed() -> void:
	_minutes_total += 1
	if _minutes_total % MINUTES_IN_DAY == 0:
		day_end.emit()
	update.emit(time_as_vector2(get_minutes_since_midnight()))


func _on_timeout():
	wait_time = global_minute
	start()
