extends Node

var time: int = 12 * 60

var debug: bool = false

enum NPCType{
	Patient,
	Guard,
	Employee,
	Other,
}

func getTime() -> String:
	var minutes_in_day = time % 1440  # Total minutes within a 24-hour period
	var hours = minutes_in_day / 60
	var minutes = minutes_in_day % 60

	# Custom function to pad the string with zeros
	var formatted_hours = pad_with_zeros(str(hours), 2)
	var formatted_minutes = pad_with_zeros(str(minutes), 2)

	return formatted_hours + ":" + formatted_minutes

# Custom function for padding with zeros
func pad_with_zeros(s: String, desired_length: int) -> String:
	while s.length() < desired_length:
		s = "0" + s
	return s


