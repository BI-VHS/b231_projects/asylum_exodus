extends Node

signal dialogue_action(dialogue_id: DialogueDB.DialogueId)

signal player_caught
signal endgame(reason: Endgame.Reason)

signal start_day
signal sampson_got_broom
#region: Inspector
signal teleport_guard_to_inspector

signal teleport_inspector_away(inspector: Node2D)
#endregion

#region: Janitor
signal vomit_created(vomit: StaticBody2D)

#endregion

#region: Quest
signal condition_done(quest: QuestHandler.QuestID, condition: String)
signal start_quest(quest: QuestHandler.QuestID)

signal show_quest_label(patient_id: PatientBase.PatientId)
signal hide_quest_label(patient_id: PatientBase.PatientId)

#endregion

#region: Journal
signal open_tab(tab: int)
signal mark_checked(index: int)
signal expose_condition(index: int)

signal create_popup(title: String, body: String)

#endregion

#region: Minigames
signal start_minigame(minigame: MinigameHandler.Minigame)
signal end_minigame(minigame: MinigameHandler.Minigame)
#endregion
