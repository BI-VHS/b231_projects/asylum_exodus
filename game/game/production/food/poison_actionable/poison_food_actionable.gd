class_name PoisonFoodActionable
extends Actionable

## Food needs to be tha parent


func action() -> void:
	printerr("Action pressed")
	get_parent().poison()
	monitorable = false
	monitoring = false
	collision_layer = 0
