class_name Food
extends Node

var is_poisoned: bool = false


func poison() -> void:
	print("Food was poisoned by someone!")
	is_poisoned = true
