class_name ProductionBuffer
extends Node2D

## Production buffer creates an interface between producent and consumer
## You only need to specify the coordinates where items can be deposited

signal item_popped(index: int)
signal item_inserted(index: int)

@export var item_consumer_nodes: Array[Node2D]
@export var item_producer_nodes: Array[Node2D]

var item_producer_positions: Array[Vector2]

var item_consumer_positions: Array[Vector2]

var item_array: Array[Sprite2D]
var item_reserved_production: Array[bool]
var item_reserved_consumption: Array[bool]


func _ready():
	for consumer_node in item_consumer_nodes:
		item_consumer_positions.push_back(consumer_node.global_position)
	for producer_node in item_producer_nodes:
		item_producer_positions.push_back(producer_node.global_position)

	for x in item_producer_positions:
		item_array.append(null)
		item_reserved_production.append(false)
		item_reserved_consumption.append(false)


## reserves an item slot, producents only
func reserve_index_production(index: int) -> void:
	item_reserved_production[index] = true


## reserves an item slot for consumption, consumers only
func reserve_index_consumption(index: int) -> void:
	item_reserved_consumption[index] = true


## inserts an item to item_array and adopts it as child, producents only
func insert(sprite: Sprite2D, index: int) -> void:
	item_inserted.emit(index)
	sprite.visible = true
	item_array[index] = sprite
	item_array[index].global_position = item_consumer_positions[index]
	add_child(item_array[index])
	item_reserved_production[index] = false


## retrieves an item from item_array and gives child for adoption, consumers only
func pop(index: int) -> Sprite2D:
	item_popped.emit(index)
	var tmp = item_array[index]
	item_array[index] = null
	remove_child(tmp)
	item_reserved_consumption[index] = false
	return tmp


## returns index of an empty cell in array, -1 if array is full, producents only
func return_empty_index() -> int:
	var size = item_array.size()
	var index = 0
	while index < size:
		if item_array[index] == null and item_reserved_production[index] == false:
			return index
		index += 1
	return -1


## returns index of a full cell in array, -1 if array is empty, consumers only
func return_full_index() -> int:
	var size = item_array.size()
	var index = 0
	while index < size:
		if item_array[index] != null and item_reserved_consumption[index] == false:
			return index
		index += 1
	return -1
