extends Control

@onready var output: RichTextLabel = $CanvasLayer/output
@onready var input: LineEdit = $CanvasLayer/input
@onready var working_directory_label: Label = $CanvasLayer/working_directory
@onready var canvas_layer: CanvasLayer = $CanvasLayer
@onready var working_directory: Node = get_tree().current_scene ## change later
@onready var testint: int = 7
@onready var testfloat: float = 7.5
@onready var teststring: String = "tadyatu"
@onready var testarray: Array = [1, 5.2, "brekeke"]
@onready var testnull = null
@onready var testdict: Dictionary = {
	1: "cus",
	2.5: "brekeke",
	"bruh": "ciusss",
}

@onready var open: bool = false

enum testenum{
	ENUM1,
	ENUM2,
	ENUM3,
}

signal emptysignal
signal signalwithargs(i: int, f: float, e: testenum, s: String)


# Called when the node enters the scene tree for the first time.
func _ready():
	print_working_directory()
	emptysignal.connect(testnoreturn)
	signalwithargs.connect(testargsretstring)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_released("console"):
		if canvas_layer.visible:
			hide_console()
		else:
			show_console()

func show_console():
	canvas_layer.visible = true
	print("open console")
	get_tree().paused = true
	open = true
	working_directory = get_tree().current_scene
	# output.text += "\n" + "Changed Directory: " + working_directory.name
	print_working_directory()
	
func hide_console():
	canvas_layer.visible = false
	open = false
	get_tree().paused = false
	print("close console")
	input.text = ""

func print_working_directory() -> String:
	working_directory_label.text = ""
	var node: Node = working_directory
	var path_array: Array[String]
	path_array.push_back(node.name)
	
	while true:
		node = node.get_parent()
		if node == null:
			break
		path_array.push_front(node.name)
	
	var text: String = "->".join(path_array)
	working_directory_label.text = text
	return text

func change_directory(args: String):
	if args == ".." and working_directory == get_tree().current_scene:
		output.text += "\n" + "Invalid Path"
		input.text = ""
		print_working_directory()
		return
	var new_directory = working_directory.get_node(args)
	if new_directory == null:
		output.text += "\n" + "Invalid Path"
		input.text = ""
		print_working_directory()
		return
	else:
		working_directory = new_directory
		var text: String = print_working_directory()
		output.text += "\n" + "Changed Directory: " + text
		input.text = ""
		return

func ls() -> String:
	var children: Array[Node] = working_directory.get_children()
	var text: String = "Children of " + working_directory.name + ":"
	for c in children:
		text += "\n" + "       "
		text += c.name
	return text
	
	
func print_single_var(args: String) -> String:
	var value = working_directory.get(args)
	if value == null:
		return "Invalid property name, or is null"
	return str(value)
	
func print_args(args: String) -> String:
	match args:
		"-v","-V":
			return print_vars()
		"-a","-A":
			return print_all()
		_:
			return print_single_var(args)
	
func print_vars() -> String: ## vars have to be strongly typed to show up
	var properties_dict = working_directory.get_property_list()
	var properties_text: String = "Properties: "
	for d in properties_dict:
		if d["usage"] == PROPERTY_USAGE_SCRIPT_VARIABLE:
			properties_text += "\n" + "      "
			properties_text += d["name"] + ": " + type_string(d["type"]) + " - " + String(d["class_name"])
	return properties_text
	
func testnoreturn():
	print("will return nothing")
	
func testnoargsretint():
	return 3
	
func testargsretstring(i: int, f: float, e: testenum, s: String):
	print(i, " ", f, " ", testenum.keys()[e], " ", s)
	return "bruh"

func print_all() -> String:
	var properties_dict = working_directory.get_property_list()
	var properties_text: String = "Properties: "
	for d in properties_dict:
		properties_text += "\n" + "      "
		properties_text += d["name"] + ": " + type_string(d["type"]) + " - " + String(d["class_name"])
		
	var methods_dict = working_directory.get_method_list()
	var methods_text: String = "Methods: "
	for d in methods_dict:
		methods_text += "\n" + "      "
		methods_text += d["name"] + ": "
		var args = d["args"]
		for a in args:
			methods_text += "\n" + "              "
			methods_text += a["name"] + ": " + type_string(a["type"]) + " - " + String(a["class_name"])
			
	var signal_dict = working_directory.get_signal_list()
	var signal_text: String = "Signals: "
	for d in signal_dict:
		signal_text += "\n" + "      "
		signal_text += d["name"] + ": "
		var args = d["args"]
		for a in args:
			signal_text += "\n" + "              "
			signal_text += a["name"] + ": " + type_string(a["type"]) + " - " + String(a["class_name"])
	return properties_text + "\n" + methods_text + "\n" + signal_text

func call_signal(args: String) -> String:
	var arg_array = args.split(" ")
	if arg_array.size() < 1:
		return "Not enough params"
		
	var callable = arg_array[0]
	if not working_directory.has_signal(callable):
		return "Working directory has no signal called " + callable
		
	if arg_array.size() == 1:
		working_directory.emit_signal(callable)
		return "Called " + callable
		
	var callable_args: Array = Array(arg_array)
	callable_args.pop_front()
	var send_args: Array
	
	if callable_args.size() > 5:
		return "Only max of 5 args is supported"
	
	for a: String in callable_args:
		if a.is_valid_float():
			send_args.push_back(a.to_float())
		elif a.is_valid_int():
			send_args.push_back(a.to_int())
		else:
			send_args.push_back(a)
			
	match send_args.size():
		0:
			working_directory.emit_signal(callable)
		1:
			working_directory.emit_signal(callable, send_args[0])
		2:
			working_directory.emit_signal(callable, send_args[0], send_args[1])
		3:
			working_directory.emit_signal(callable, send_args[0], send_args[1], send_args[2])
		4: 
			working_directory.emit_signal(callable, send_args[0], send_args[1], send_args[2], send_args[3])
		5:
			working_directory.emit_signal(callable, send_args[0], send_args[1], send_args[2], send_args[3], send_args[4])
	return "Called " + callable + " with " + str(send_args.size()) + " arguments"
	
func call_method(args: String) -> String:
	var arg_array = args.split(" ")
	if arg_array.size() < 1:
		return "Not enough params"
		
	var callable = arg_array[0]
	if not working_directory.has_method(callable):
		return "Working directory has no method called " + callable
	
	if arg_array.size() == 1:
		return str(working_directory.call(callable))
	
	var callable_args: Array = Array(arg_array)
	callable_args.pop_front()
	var send_args: Array
	for a: String in callable_args:
		if a.is_valid_float():
			send_args.push_back(a.to_float())
		elif a.is_valid_int():
			send_args.push_back(a.to_int())
		else:
			send_args.push_back(a)
			
	return str(working_directory.callv(callable, send_args))
	
func help() -> String:
	return "Will maybe implement later"
	
func save() -> String:
	var path: String = OS.get_executable_path()
	var disjointed = path.split("/")
	disjointed = Array(disjointed)
	disjointed.pop_back()
	path = "/".join(disjointed)
	var version: int = 0
	while FileAccess.file_exists(path + "/log" + str(version) + ".txt"):
		version += 1
	
	var file = FileAccess.open(path + "/log" + str(version) + ".txt", FileAccess.WRITE)
	file.store_string(output.text)
	return "Saved console output to log file"

func parser(text: String):
	var parsed = text.split(" ", false, 1)
	if parsed.size() < 2:
		match parsed[0]:
			"empty":	## empty output
				output.text = ""
				input.text = ""
				print_working_directory()
				return
			"help":		## print help
				output.text += "\n" + help()
				input.text = ""
				print_working_directory()
				return
			"save":		## save output to log file
				output.text += "\n" + save()
				input.text = ""
				print_working_directory()
				return
			"ls":		## print all children nodes
				output.text += "\n" + ls()
				input.text = ""
				print_working_directory()
				return
			#"print":	## print all vars
			#	output.text += "\n" + print_vars()
			#	input.text = ""
			#	print_working_directory()
			#	return
			_:
				output.text += "\n" + "Not enough arguments or Invalid command"
				input.text = ""
				print_working_directory()
				return

	var command: String = parsed[0]
	var args: String = parsed[1]
	
	match command:
		"cd": 		## change directory (node)
			change_directory(args)
		"call":  	## call current node method
			output.text += "\n" + call_method(args)
			input.text = ""
			print_working_directory()
			return
		"print":	## print current node var, -A print all, -V print vars
			output.text += "\n" + print_args(args)
			input.text = ""
			print_working_directory()
			return
		"set":		## set variable
			output.text += "\n" + "Will maybe implement later"
			input.text = ""
			print_working_directory()
			return
		"emit":
			output.text += "\n" + call_signal(args)	## emit current node signal
			input.text = ""
			print_working_directory()
			return
			
	# output.text += "\n" + "\n".join(parsed)
	input.text = ""
	print_working_directory()

func _on_input_text_submitted(new_text):
	if new_text != "":
		parser(new_text)
	else:
		output.text += "\n" + new_text
		input.text = ""
		print_working_directory()
