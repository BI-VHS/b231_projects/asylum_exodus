@tool
class_name MinigameHandler
extends Node2D

@onready var lockpick = $CanvasLayer/Lockpick
@onready var hexcode = $CanvasLayer/Hexcode
@onready var breakerbox = $CanvasLayer/Breakerbox
@onready var fence = $CanvasLayer/Fence
@export var player: Player
@export var door_collider: CollisionShape2D
@export var guard: NewBaseGuard
@onready var fence_active: bool = true

enum Minigame{
	Lockpick,
	Hexcode,
	Breakerbox,
	Fence,
}

var MinigameInit = {
	Minigame.Lockpick: lockpick_init,
	Minigame.Hexcode: hexcode_init,
	Minigame.Breakerbox: breakerbox_init,
	Minigame.Fence: fence_init,
}

var MinigameEnd = {
	Minigame.Lockpick: lockpick_end,
	Minigame.Hexcode: hexcode_end,
	Minigame.Breakerbox: breakerbox_end,
	Minigame.Fence: fence_end,
}

# Called when the node enters the scene tree for the first time.
func _ready():
	MessageBus.start_minigame.connect(start_minigame)
	MessageBus.end_minigame.connect(end_minigame)
	lockpick.hide()

func start_minigame(minigame: Minigame):
	MinigameInit[minigame].call()
	
func end_minigame(minigame: Minigame):
	MinigameEnd[minigame].call()

func lockpick_init():
	lockpick.show()
	if global_variables.debug:
		$CanvasLayer/Lockpick/Label.show()
	get_tree().paused = true
	
func hexcode_init():
	hexcode.show()
	MessageBus.create_popup.emit("Podmínka splněna", "Propliž se k boudě s jističi")
	get_tree().paused = true

func breakerbox_init():
	$CanvasLayer/Breakerbox/Label.text += str(QuestHandler.student_breaker)
	print(QuestHandler.correct_breaker)
	breakerbox.show()
	get_tree().paused = true
	
func fence_init():
	fence.fence_active = fence_active
	fence.show()
	get_tree().paused = true
	
func lockpick_end():
	lockpick.hide()
	get_tree().paused = false
	player.teleport_to(Vector2(-10274, 262))
	MessageBus.mark_checked.emit(0)
	MessageBus.expose_condition.emit(1)
	MessageBus.expose_condition.emit(2)
	MessageBus.create_popup.emit("Podmínka splněna", "Odemkni hlavní dveře")
	var hexcode_trigger = MinigameActionable.new()
	hexcode_trigger.constructor(Vector2(-9774,684), get_tree().current_scene, "Otevřít dveře", MinigameHandler.Minigame.Hexcode, true)
	print("guard moving", guard.global_position)
	guard.global_position = Vector2(-11793, 341)
	print(guard.global_position)
	
func hexcode_end():
	hexcode.hide()
	get_tree().paused = false
	door_collider.queue_free()
	MessageBus.mark_checked.emit(1)
	MessageBus.mark_checked.emit(2)
	MessageBus.create_popup.emit("Podmínka splněna", "Otevři dveře od boudy")
	MessageBus.expose_condition.emit(3)
	var breakerbox_trigger = MinigameActionable.new()
	breakerbox_trigger.constructor(Vector2(-9480,641), get_tree().current_scene, "Vyhodit jistič", MinigameHandler.Minigame.Breakerbox, true)

func breakerbox_end():
	breakerbox.hide()
	get_tree().paused = false
	if breakerbox.disabled_breaker == QuestHandler.correct_breaker:
		fence_active = false
	elif breakerbox.disabled_breaker == 0:
		print("breakerbox is set on 0")
	MessageBus.mark_checked.emit(3)
	MessageBus.create_popup.emit("Podmínka splněna", "Vyhoď jistič")
	MessageBus.expose_condition.emit(4)
	MessageBus.expose_condition.emit(5)
	var fence_trigger = MinigameActionable.new()
	fence_trigger.constructor(Vector2(-8579,471), get_tree().current_scene, "Přestřihnout plot", MinigameHandler.Minigame.Fence, true)
	
func fence_end():
	fence.hide()
	# get_tree().paused = false
	if fence.fence_active == true: # death by electricity
		MessageBus.endgame.emit(Endgame.Reason.Death)
	else:
		MessageBus.endgame.emit(Endgame.Reason.Freedom) # freedom
