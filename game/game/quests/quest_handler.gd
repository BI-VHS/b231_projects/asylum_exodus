extends Node

## Quests have condition dicts, init funcs and have to be registered in QUESTID,
## all_quests_conditions, all_quests_done and all_quests_init
## for now, quests only have conditions that all have to be completed

var debug: bool = global_variables.debug

## for dialogues, says what quest should come next
var next_quest = 1
var current_quest = 0

@onready var rng: RandomNumberGenerator = RandomNumberGenerator.new()
var correct_breaker: int
var false_breaker: int
var maybe_breaker: int
@onready var student_breaker: int = 0

var quest1 = {
	"wake_up": false,
	"leave": false,
	"talk_first": false,
	"poison": false,
	"talk_second": false,
}
var quest2 = {
	"talk_first": false,
	"lure_out": false,
	"sneak_in": false,
	"take_broom": false,
	"talk_second": false,
}
var quest3 = {
	"talk_first": false,
	"find_remote": false,
	# "ask_sampson": false,
	"get_remote": false,
	"talk_second": false,
}
var quest4 = {
	"talk_first": false,
	"sneak_in": false,
	"find_pill": false,
	"return": false,
}
var quest5 = {
	"take_plans": false,
	"talk_to_eric": false,
	"talk_to_student": false,
}
var quest6 = {
	"talk_to_eric": false,
	"talk_to_sampson": false,
	"take_lockpick": false,
	"take_pliers": false,
}
var quest7 = {
	"open_main_doors": false,
	"open_shed_doors": false,
	"turn_off_fence": false,
	"cut_fence": false,
	"freedom": false,
}

enum QuestID {
	QUEST1,
	QUEST2,
	QUEST3,
	QUEST4,
	QUEST5,
	QUEST6,
	QUEST7,
}

var all_quests_conditions = {
	QuestID.QUEST1: quest1,
	QuestID.QUEST2: quest2,
	QuestID.QUEST3: quest3,
	QuestID.QUEST4: quest4,
	QuestID.QUEST5: quest5,
	QuestID.QUEST6: quest6,
	QuestID.QUEST7: quest7,
}

var all_quests_done = {
	QuestID.QUEST1: false,
	QuestID.QUEST2: false,
	QuestID.QUEST3: false,
	QuestID.QUEST4: false,
	QuestID.QUEST5: false,
	QuestID.QUEST6: false,
	QuestID.QUEST7: false,
}

var all_quests_init = {
	QuestID.QUEST1: quest1_init,
	QuestID.QUEST2: quest2_init,
	QuestID.QUEST3: quest3_init,
	QuestID.QUEST4: quest4_init,
	QuestID.QUEST5: quest5_init,
	QuestID.QUEST6: quest6_init,
	QuestID.QUEST7: quest7_init,
}

func reset_quest_handler():
	for d in all_quests_conditions.values():
		for cond in d.values():
			cond = false
	student_breaker = 0
	correct_breaker = rng.randi_range(1,3)
	false_breaker = (correct_breaker % 3) + 1
	var decision: int = rng.randi_range(0,1)
	maybe_breaker = decision*correct_breaker + (1 - decision)*false_breaker
	current_quest = 0
	next_quest = 1

func next_quest_label(index: int):
	match index:
		2:
			MessageBus.show_quest_label.emit(PatientBase.PatientId.SAMPSON)
		3:
			MessageBus.show_quest_label.emit(PatientBase.PatientId.NIGMA)
		4:
			MessageBus.show_quest_label.emit(PatientBase.PatientId.SOFTING)
		5:
			pass
		6:
			MessageBus.show_quest_label.emit(PatientBase.PatientId.NIGMA)
		7:
			pass

func _ready():
	MessageBus.condition_done.connect(condition_done)
	MessageBus.start_quest.connect(quest_start_fork)
	correct_breaker = rng.randi_range(1,3)
	false_breaker = (correct_breaker % 3) + 1
	var decision: int = rng.randi_range(0,1)
	maybe_breaker = decision*correct_breaker + (1 - decision)*false_breaker
	
## checks all conditions of a quest, returns true, if they are all true
func check_quest_done(quest: QuestID) -> bool:
	var conditions = all_quests_conditions[quest]
	for cond in conditions:
		if conditions[cond] == false:
			return false
	return true
	
## signal func for completing a condition
func condition_done(quest: QuestID, condition: String):
	(all_quests_conditions[quest])[condition] = true
	print(all_quests_conditions[quest])
	if quest == QuestID.QUEST5 and condition == "take_plans": ## special case for monologue
		MessageBus.dialogue_action.emit(DialogueDB.DialogueId.MONOLOGUE)
	if check_quest_done(quest) == true:
		all_quests_done[quest] = true
		if all_quests_done[QuestID.QUEST6] == true: ## check for endgame, for now quest 6 is last
			# endgame()
			quest7_init()
			MessageBus.create_popup.emit("Poslední Příprava", "Quest splněn")
			MessageBus.create_popup.emit("Nyní se můžeš pokusit o útěk", "Začni u hlavních dveří")
			next_quest += 1
			next_quest_label(next_quest)
			current_quest = 0
			return
		next_quest += 1
		next_quest_label(next_quest)
		current_quest = 0
		MessageBus.open_tab.emit(0)
		print("quest", quest+1, " done, next quest: quest", next_quest)

## signal func for starting quests
func quest_start_fork(quest: QuestID):
	all_quests_init[quest].call()
	
# fill in inits
func quest1_init():
	print("quest1 init")
	MessageBus.open_tab.emit(1)
	MessageBus.mark_checked.emit(0)
	current_quest = 1
	
	# move to quest7 init when not debugging
	if debug:
		var lockpick_trigger = MinigameActionable.new()
		lockpick_trigger.constructor(Vector2(-311, 1683), get_tree().current_scene, "Začít útěk", MinigameHandler.Minigame.Lockpick, true)
	

	
func quest2_init():
	print("quest2 init")
	MessageBus.open_tab.emit(2)
	MessageBus.hide_quest_label.emit(PatientBase.PatientId.SAMPSON)
	current_quest = 2
	# spawn broom in the correct position
	var broom = PickupActionable.new()
	broom.constructor("res://game/items/static/broom.tscn", Vector2(744, 1379), QuestID.QUEST2, "take_broom", "Vzít koště", get_tree().current_scene, 4, 3, false, true, "Podmínka splněna", "Vezmi koště")
	
func quest3_init():
	print("quest3 init")
	MessageBus.open_tab.emit(3)
	MessageBus.hide_quest_label.emit(PatientBase.PatientId.NIGMA)
	current_quest = 3
	# spawn remote
	var remote = PickupActionable.new()
	remote.constructor("res://game/items/static/remote.tscn", Vector2(10523, 1690), QuestID.QUEST3, "get_remote", "Vzít ovladač", get_tree().current_scene, 3, 2, false, true, "Podmínka splněna", "Seber ovladač")
	
func quest4_init():
	print("quest4 init")
	MessageBus.open_tab.emit(4)
	MessageBus.hide_quest_label.emit(PatientBase.PatientId.SOFTING)
	current_quest = 4
	# spawn pill
	var pill = PickupActionable.new()
	pill.constructor("res://game/items/static/viagra.tscn", Vector2(12144, -1150), QuestID.QUEST4, "find_pill", "Vzít pilulku", get_tree().current_scene, 4, 3, false, true, "Podmínka splněna", "Vem pilulku")
	
func quest5_init():
	print("quest5 init")
	MessageBus.open_tab.emit(5)
	current_quest = 5
	# spawn notebook
	var notebook = PickupActionable.new()
	notebook.constructor("res://game/items/static/notebook.tscn", Vector2(2200, 1493), QuestID.QUEST5, "take_plans", "Vzít plány", get_tree().current_scene, 1, 0, false, true, "Podmínka splněna", "Seber plány z archivu")
	
func quest6_init():
	print("quest6 init")
	MessageBus.open_tab.emit(6)
	MessageBus.hide_quest_label.emit(PatientBase.PatientId.NIGMA)
	current_quest = 6
	# spawn pliers and lockpick
	var lockpick = PickupActionable.new()
	var pliers = PickupActionable.new()
	
	lockpick.constructor("res://game/items/static/lockpick.tscn", Vector2(1760, -1261), QuestID.QUEST6, "take_lockpick", "Vzít šperhák", get_tree().current_scene, -1, 3, false, true, "Podmínka splněna", "Vem šperhák")
	pliers.constructor("res://game/items/static/pliers.tscn", Vector2(744, 1379), QuestID.QUEST6, "take_pliers", "Vzít štípačky", get_tree().current_scene, -1, 2, false, true, "Podmínka splněna", "Vem štípací kleště")
	
func quest7_init():
	print("quest7 init")
	MessageBus.open_tab.emit(7)
	current_quest = 7
	
	if not debug:
		var lockpick_trigger = MinigameActionable.new()
		lockpick_trigger.constructor(Vector2(-311, 1683), get_tree().current_scene, "Začít útěk", MinigameHandler.Minigame.Lockpick, true)

	
func endgame():
	# todo spawn guards
	
	var guard1 = Sprite2D.new()
	guard1.texture = ResourceLoader.load("res://sprites/characters/guards/guard_01/guard_variant_v1.png")
	var guard2 = Sprite2D.new()
	guard2.texture = ResourceLoader.load("res://sprites/characters/guards/guard_02/guard_variant_v2.png")
	
	guard1.global_position = Vector2(11280, -1016)
	guard2.global_position = Vector2(11280, -900)
	guard1.scale = Vector2(5, 5)
	guard2.scale = Vector2(5, 5)
	
	
	# get_tree().current_scene.add_child(guard1)
	# get_tree().current_scene.add_child(guard2)
	
	await get_tree().create_timer(3).timeout
	MessageBus.endgame.emit()
