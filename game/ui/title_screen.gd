extends Control


const START_SCENE: String = "res://game/levels/ground_floor.tscn"

@onready
var click_sound_player: AudioStreamPlayer = $ClickSoundPlayer

func _ready():
	$ContentBackground/ContentMargin/Content/Buttons/Start.grab_focus()


func _play_click_sound():
	click_sound_player.play()


func _on_start_pressed():
	_play_click_sound()
	get_tree().change_scene_to_file(START_SCENE)


func _on_quit_pressed():
	_play_click_sound()
	await click_sound_player.finished
	get_tree().quit()
