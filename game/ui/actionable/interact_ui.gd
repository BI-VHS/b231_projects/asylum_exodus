class_name InteractUI
extends CanvasLayer

const MARGIN_RIGHT: float = 32
const MARGIN_BOTTOM: float = 32

@export var font_size: int = 24

@onready var text: Label = %Text
@onready var fake_text: Label = %FakeText
@onready var panel: Panel = %Panel
@onready var text_margin: float = %MarginContainer.get_theme_constant("margin_bottom")


func _ready() -> void:
	text.add_theme_font_size_override("font_size", font_size)
	fake_text.add_theme_font_size_override("font_size", font_size)


func show_interaction(_actionable: Actionable) -> void:
	panel.show()
	fake_text.show()

	if _actionable is DialogueActionable:
		text.text = "Promluv si s: " + _actionable.interaction_name
	elif _actionable is PoisonFoodActionable:
		text.text = "Otrav: " + _actionable.interaction_name
	else:
		text.text = "Akce: " + _actionable.interaction_name

	fake_text.text = text.text  # let fake text render and use its size for real text
	await get_tree().process_frame

	var text_x: float = fake_text.size.x
	var text_y: float = fake_text.size.y

	var viewport_x: float = get_viewport().get_visible_rect().size.x
	var viewport_y: float = get_viewport().get_visible_rect().size.y

	panel.size.x = text_x + 2 * text_margin
	panel.size.y = text_y + 2 * text_margin

	panel.position.y = viewport_y - panel.size.y - MARGIN_BOTTOM
	panel.position.x = viewport_x - panel.size.x - MARGIN_RIGHT

	fake_text.hide()


func hide_interaction() -> void:
	panel.hide()
