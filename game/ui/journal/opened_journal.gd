@tool
extends Control


signal journal_closed

@export var quest_tabs: TabContainer
@onready var check_texture: Texture2D = preload("res://sprites/icons/icon_checked.png")

func _ready():
	MessageBus.open_tab.connect(show_quest)
	MessageBus.mark_checked.connect(mark_checked_condition)
	MessageBus.expose_condition.connect(expose_condition)

func _on_close_button_pressed():
	journal_closed.emit()
	
func show_quest(tab: int):
	quest_tabs.current_tab = tab
	
func mark_checked_condition(index: int):
	var child = quest_tabs.get_current_tab_control()
	var list = child.get_node("ItemList")
	list.set_item_icon(index, check_texture)
	
func expose_condition(index: int):
	var child = quest_tabs.get_current_tab_control()
	var list: ItemList = child.get_node("ItemList")
	var source: ItemList = child.get_node("SourceList")
	var text = source.get_item_text(index)
	var icon = source.get_item_icon(index)
	list.add_item(text, icon, false)
	
func _on_button_pressed():
	get_tree().quit()


func _on_button_2_pressed():
	QuestHandler.reset_quest_handler()
	GlobalTime.reset_global_time()
	get_tree().paused = false
	get_tree().change_scene_to_file("res://game/levels/Main.tscn")
