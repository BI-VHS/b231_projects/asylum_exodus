extends Control

@onready
var closed_journal: TextureButton = $ClosedJournal
@onready
var opened_journal := $OpenedJournal

func _process(delta):
	if !Input.is_action_just_pressed("Journal"):
		return
	
	if Console.open:
		return
	
	if $OpenedJournal.is_visible():
		close_journal()
		return
		
	open_journal()
	

func open_journal():
	$PagesSound.play()
	$ClosedJournal.hide()
	opened_journal.show()
	get_tree().paused = true


func close_journal():
	$ClosedJournal.show()
	opened_journal.hide()
	get_tree().paused = false


func _on_closed_journal_pressed():
	open_journal()


func _on_opened_journal_journal_closed():
	close_journal()
