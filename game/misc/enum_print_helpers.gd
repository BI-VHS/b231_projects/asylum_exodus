class_name EnumPrintHelpers
extends Object


static func name_by_id(id: int, enumeration: Dictionary) -> String:
	for str: String in enumeration:
		if enumeration[str] == id:
			return str
	return ""
