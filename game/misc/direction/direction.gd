extends Resource

## Represents a direction, can convert to/from normalized Vector2 with the quirk
## that (0,0) -> DOWN and (x,y), where x==y -> UP/DOWN based on sign of y
class_name Direction

enum DirectionEnum { LEFT, RIGHT, UP, DOWN }

@export var direction: DirectionEnum


func _init(_direction: DirectionEnum):
	direction = _direction


## Conversion function to normalized vector
func to_normalized_vector() -> Vector2:
	match direction:
		DirectionEnum.LEFT:
			return Vector2(-1, 0)
		DirectionEnum.RIGHT:
			return Vector2(1, 0)
		DirectionEnum.UP:
			return Vector2(0, -1)
		DirectionEnum.DOWN:
			return Vector2(0, 1)
		_:
			assert(false)
			return Vector2(0, 0)


## Works the same as `max_axis_index`, but returns AXIS_Y, if equal, because of
## preference to show up/down animations when idle
static func _max_axis_index_defaults_to_y(vector: Vector2) -> int:
	if abs(vector.x) == abs(vector.y):
		return Vector2.AXIS_Y
	else:
		return vector.max_axis_index()


## Conversion function, with special case Vector(0, 0) -> DOWN
## and when x and y absolute are the same defaults to Y axis
static func vector_to_direction(vector: Vector2) -> Direction:
	match _max_axis_index_defaults_to_y(vector.abs()):
		Vector2.AXIS_X:
			return Direction.new(DirectionEnum.LEFT if vector.x < 0 else DirectionEnum.RIGHT)
		Vector2.AXIS_Y:  # when Vector(0,0) -> DOWN
			return Direction.new(DirectionEnum.UP if vector.y < 0 else DirectionEnum.DOWN)
		_:
			return Direction.new(DirectionEnum.DOWN)


static func new_left() -> Direction:
	return Direction.new(DirectionEnum.LEFT)


static func new_right() -> Direction:
	return Direction.new(DirectionEnum.RIGHT)


static func new_up() -> Direction:
	return Direction.new(DirectionEnum.UP)


static func new_down() -> Direction:
	return Direction.new(DirectionEnum.DOWN)


func is_left() -> bool:
	return direction == DirectionEnum.LEFT


func is_right() -> bool:
	return direction == DirectionEnum.RIGHT


func is_up() -> bool:
	return direction == DirectionEnum.UP


func is_down() -> bool:
	return direction == DirectionEnum.DOWN


func is_horizontal() -> bool:
	return direction == DirectionEnum.LEFT || direction == DirectionEnum.RIGHT


func is_vertical() -> bool:
	return direction == DirectionEnum.UP || direction == DirectionEnum.DOWN
