class_name IntRange
extends Resource

var from: int

var to: int


func _init(_from: int, _to: int) -> void:
	assert(_to >= from)
	from = _from
	to = _to


func contains_left_inclusive(what: int) -> bool:
	return from <= what and what < to


func contains_inclusive(what: int) -> bool:
	return from <= what and what <= to
