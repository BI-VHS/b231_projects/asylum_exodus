class_name DialogueDB
extends RefCounted

## Maps dialogue ids onto DalogueContainers, use of DialogueId enum instead
## of Strings is to help with autocompletion and to be less error-prone.

enum DialogueId {
	EXAMPLE,
	SOFTING,
	NIGMA,
	SAMPSON,
	PA_STUDENT,
	RAINMAN,
	PUBLIC_TRANSPORT_INSPECTOR,
	WASHING_MACHINE,
	SEXY_VEGETARIAN,
	MONOLOGUE,
}

## Dialogues -> DialogueContainer
var _db: Dictionary = {}


func get_dialogue(dialogue_id: DialogueId) -> DialogueContainer:
	return _db[dialogue_id]


func add_dialogue(dialogue_container: DialogueContainer) -> void:
	_db[dialogue_container.dialogue_id] = dialogue_container
