class_name DialogueContainer
extends RefCounted

## Holds all necessary data for a dialogue: the id, context and resource itself.

var dialogue_id: DialogueDB.DialogueId

var dialogue_resource: DialogueResource

var dialogue_context: DialogueContext

var start_title: String:
	get:
		return dialogue_context.start_title


func _init(
	_dialogue_id: DialogueDB.DialogueId,
	_dialogue_resource: DialogueResource,
	_dialogue_context: DialogueContext
) -> void:
	dialogue_id = _dialogue_id
	dialogue_resource = _dialogue_resource
	dialogue_context = _dialogue_context
