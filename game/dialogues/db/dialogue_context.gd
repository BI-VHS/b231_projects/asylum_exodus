class_name DialogueContext
extends RefCounted

## Base class for individual dialogue contexts - object which offers variables
## and functions to call in .dialogue files. The idea is to have a class responsible
## for interfacing with the dialogue and game as a whole.

## If a context should decide on start_title based on its state define a
## 'get_start_title' function, otherwise set it just like any other variable
## and you will get back the stuff you put there.
var start_title: String:
	get:
		if has_method("get_start_title"):
			return call("get_start_title")
		return _backing_start_title
	set(title):
		_backing_start_title = title

var _backing_start_title: String = ""

var quest_handler = QuestHandler
