extends Node2D

@export var dialogue_resource: DialogueResource
@export var dialogue_title: String = ""

@onready var dialogue_controller: DialogueController = $"%DialogueController"


func _ready() -> void:
	dialogue_controller.start_dialogue_with_resource(dialogue_resource, dialogue_title)
