extends Node2D

@export var dialogue_controller: DialogueController
@export var pa_student_dialogue_resource: DialogueResource
@export var rainman_dialogue_resource: DialogueResource
@export var inspector_dialogue_resource: DialogueResource


func _ready() -> void:
	var pa_student_container := DialogueContainer.new(
		DialogueDB.DialogueId.PA_STUDENT,
		pa_student_dialogue_resource,
		PaStudentDialogueContext.new()
	)

	var rainman_container := DialogueContainer.new(
		DialogueDB.DialogueId.RAINMAN, rainman_dialogue_resource, DialogueContext.new()
	)

	var inspector_container := DialogueContainer.new(
		DialogueDB.DialogueId.PUBLIC_TRANSPORT_INSPECTOR,
		inspector_dialogue_resource,
		PublicTransportInspectorContext.new($Inspector)
	)

	dialogue_controller.add_dialogue(pa_student_container)
	dialogue_controller.add_dialogue(rainman_container)
	dialogue_controller.add_dialogue(inspector_container)
