class_name PublicTransportInspectorContext
extends DialogueContext

## Reference to inspector
var inspector: Node2D


func _init(_inspector: Node2D) -> void:
	inspector = _inspector


func call_guard():
	print("Inspector call_guard called")
	MessageBus.teleport_guard_to_inspector.emit()


func walk_away():
	print("Inspector walk_away called")
	MessageBus.teleport_inspector_away.emit()
