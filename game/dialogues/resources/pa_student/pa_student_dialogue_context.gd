class_name PaStudentDialogueContext
extends DialogueContext

const PROGTEST_SOLVE_TIMEOUT: float = 5

var gratitudes_given: bool = false

var gave_advice: bool = false

var progtest_solved: bool = false

var angry: bool = false
var progtest_attempted: bool = false

#var quest_handler = QuestHandler

func advice_given() -> void:
	print("Advice_given")
	gave_advice = true
	await MessageBus.get_tree().create_timer(5, false).timeout
	print("Progtest solved")
	progtest_solved = true
