class_name DialogueController
extends Node2D

## Responsible for everything concerning dialogue system.
## Most commonly used for playing dialogue by DialogueId.
## Notes: The layers are ok in my opinion, but inicialization of the dialogues
##        is likely going to require some sort of DialogueFactory and it might
##        be difficult to connect signals from DialogueContexts to whatever responds to it.
##        So far an ok way to circumvent it is to use MessageBus at the cost
##        of some global state e.g. inspector dialogue.

const Balloon = preload("res://ui/dialogue/balloon.tscn")

var dialogue_db: DialogueDB = DialogueDB.new()

var balloon: DialogueBalloon


func add_dialogue(dialogue_container: DialogueContainer):
	print("Adding dialogue")
	dialogue_db.add_dialogue(dialogue_container)


func _ready() -> void:
	MessageBus.dialogue_action.connect(start_dialogue_by_id)


## Does not work with any context, use for simple dialogues only or testing
func start_dialogue_with_resource(dialogue_resource: DialogueResource, title: String = "") -> void:
	_init_baloon()
	balloon.start(dialogue_resource, title)


## Gets DialogueContainer by its id from DB and plays it
func start_dialogue_by_id(dialogue_id: DialogueDB.DialogueId) -> void:
	print("Starting dialogue by id: ", dialogue_id)
	start_dialogue_with_container(dialogue_db.get_dialogue(dialogue_id))


## Plays DialogueContainer
func start_dialogue_with_container(dialogue_container: DialogueContainer) -> void:
	_init_baloon()
	balloon.start(
		dialogue_container.dialogue_resource,
		dialogue_container.start_title,
		[dialogue_container.dialogue_context]
	)


func _init_baloon():
	balloon = Balloon.instantiate()
	add_child(balloon)
	balloon.dialogue_finished.connect(_on_dialogue_finished)


func _on_dialogue_finished():
	get_tree().paused = false
