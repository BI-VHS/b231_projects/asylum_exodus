# Asylum Exodus

## Links
[Asset table](https://docs.google.com/spreadsheets/d/1ft4GlGCO0k-PwpSDS-T5OM98Ca0d7UwA_0zYxCSO1MY/edit?usp=sharing)

[VHS game review table](https://docs.google.com/spreadsheets/d/1aGXxLCR9rOStOHh2Vf4RhgCRHSzR7CpRUi3JCR7897w/edit?usp=sharing)

[Dokument s dialogy](https://docs.google.com/document/d/1m8oynouRXTj1BAwksshWxYChktngra2Dvf_ECzwoWro/edit?usp=sharing)

[Dokument s questy](https://docs.google.com/document/d/1LYBiHZJxT5wozMfn-9UG26p0PBcddE13CarQgLnfY_s/edit?usp=sharing)

## Introduction
Asylum Exodus is a semestral project for the BI-VSH course at FIT ČVUT. It is a 2D top-down story-driven game made in the Godot game engine set in a mental asylum. The player assumes the role of an unstable mental patient and is tasked with either trying to escape or trying to recover from their mental health issues

## Download and Run
You can download the game from pipeline jobs, select your OS and download the artifacts. Unzip and play the game. Warning!
For yet unknown reason, interaction does not work in compiled game, for the time being, please download the export folder and launch
the executable. This executable was compiled locally which made it work just fine on my computer (Win10), hopefully, it will work
on your computer as well. If that does not work either, then download newest Godot build, and launch it from the editor, if that does not work either, then I am going to pull all my hair out...

## Documentation links
[Asylum Exodus Wiki](https://gitlab.fit.cvut.cz/BI-VHS/b231_projects/asylum_exodus/-/wikis/Asylum-Exodus)

## Godot stadnard guidelines

[Godot project organization guidelines](https://docs.godotengine.org/en/stable/tutorials/best_practices/project_organization.html)
\
[GDScript style guide](https://docs.godotengine.org/en/stable/tutorials/scripting/gdscript/gdscript_styleguide.html)
